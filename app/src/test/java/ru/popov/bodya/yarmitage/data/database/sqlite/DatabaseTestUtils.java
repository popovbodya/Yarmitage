package ru.popov.bodya.yarmitage.data.database.sqlite;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertNotNull;

/**
 * @author popovbodya
 */

class DatabaseTestUtils {

    /**
     * Проверяется существование в Базе данных таблицы с определенными столбцами
     *
     * @param database        База данных
     * @param expectedTable   название таблицы
     * @param expectedColumns массив названий столбцов
     */
    static void assertTableExists(SQLiteDatabase database, String expectedTable, String[] expectedColumns) {
        Cursor cursor = null;
        try {
            cursor = database.query(expectedTable, null, null, null, null, null, null);
            assertNotNull("Cursor for table " + expectedTable + " cannot be null", cursor);
            assertEquals("Column count in table " + expectedTable + " is wrong", expectedColumns.length, cursor.getColumnCount());
            for (String column : expectedColumns) {
                int columnIndex = cursor.getColumnIndex(column);
                String errorMessage = "Column " + column + " should present in table " + expectedTable;
                assertEquals(errorMessage, true, columnIndex >= 0);
            }
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
    }

}
