package ru.popov.bodya.yarmitage.data.repositories;

import android.content.Intent;

import com.yandex.authsdk.YandexAuthException;
import com.yandex.authsdk.YandexAuthToken;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import io.reactivex.Completable;
import io.reactivex.Single;
import ru.popov.bodya.yarmitage.data.database.preferences.SharedPreferencesWrapper;
import ru.popov.bodya.yarmitage.data.network.api.sdk.YandexSdkWrapper;
import ru.popov.bodya.yarmitage.domain.auth.AuthRepository;

import static org.mockito.Mockito.when;

/**
 * @author popovbodya
 */
@RunWith(MockitoJUnitRunner.class)
public class AuthRepositoryImplTest {

    @Mock
    private SharedPreferencesWrapper mSharedPreferencesWrapper;
    @Mock
    private YandexSdkWrapper mYandexSdkWrapper;

    private AuthRepository mAuthRepository;

    @Before
    public void setUp()  {
        mAuthRepository = new AuthRepositoryImpl(mSharedPreferencesWrapper, mYandexSdkWrapper);
    }

    @Test
    public void testGetAuthIntent() {

        final Intent intent = new Intent();
        when(mYandexSdkWrapper.getAuthIntent()).thenReturn(Single.just(intent));

        mAuthRepository
                .getAuthIntent()
                .test()
                .assertResult(intent);
    }

    @Test
    public void testExtractToken() throws YandexAuthException {

        final int result = -1;
        final Intent intent = new Intent();
        final YandexAuthToken yandexAuthToken = new YandexAuthToken("qwerty1235", 12345678911L);
        when(mYandexSdkWrapper.extractToken(result, intent)).thenReturn(Single.just(yandexAuthToken));

        mAuthRepository
                .extractToken(result, intent)
                .test()
                .assertResult(yandexAuthToken);
    }

    @Test
    public void testSaveToken() {

        final YandexAuthToken yandexAuthToken = new YandexAuthToken("qwerty1235", 12345678911L);
        when(mSharedPreferencesWrapper.saveToken(yandexAuthToken)).thenReturn(Completable.complete());

        mAuthRepository
                .saveToken(yandexAuthToken)
                .test()
                .assertComplete()
                .assertNoErrors();
    }
}