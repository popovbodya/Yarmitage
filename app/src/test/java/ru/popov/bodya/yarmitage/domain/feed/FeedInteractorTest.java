package ru.popov.bodya.yarmitage.domain.feed;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.Single;
import ru.popov.bodya.yarmitage.app.rx.RxSchedulers;
import ru.popov.bodya.yarmitage.app.rx.RxSchedulersStub;
import ru.popov.bodya.yarmitage.domain.global.models.Resource;

import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * @author popovbodya
 */
@RunWith(MockitoJUnitRunner.class)
public class FeedInteractorTest {

    @Mock
    private FeedRepository mFeedRepository;

    private RxSchedulers mRxSchedulers;
    private FeedInteractor mFeedInteractor;

    @Before
    public void setUp() {
        mRxSchedulers = spy(new RxSchedulersStub());
        mFeedInteractor = new FeedInteractor(mFeedRepository, mRxSchedulers);
    }


    @Test
    public void testGetImages() {

        final int defaultLimit = 20;
        final int page = 3;
        final List<Resource> resourceList = new ArrayList<>();

        when(mFeedRepository.getFilesFromNetwork(defaultLimit * page, defaultLimit, false)).thenReturn(Single.just(resourceList));

        //noinspection unchecked
        mFeedInteractor
                .getImages(page)
                .test()
                .assertResult(resourceList);
    }


    @Test
    public void testInitialFetchDataFromAllSources() {

        final int initialOffest = 0;
        final int defaultLimit = 20;

        final List<Resource> resourceListFromCache = createResourceList(3);
        final List<Resource> resourceListFromNetwork = createResourceList(8);

        when(mFeedRepository.getFilesFromCache()).thenReturn(Single.just(resourceListFromCache));
        when(mFeedRepository.getFilesFromNetwork(initialOffest, defaultLimit, true)).thenReturn(Single.just(resourceListFromNetwork));

        //noinspection unchecked
        mFeedInteractor
                .initialFetchDataFromAllSources()
                .test()
                .assertResult(resourceListFromNetwork, resourceListFromCache);

        verify(mRxSchedulers, times(2)).getIOScheduler();
    }

    private List<Resource> createResourceList(int size) {
        final List<Resource> resourceList = new ArrayList<>();
        for (int i = 0; i < size; i++) {
            resourceList.add(createResource(i));
        }
        return resourceList;
    }

    private Resource createResource(int id) {
        return new Resource("resourceId" + id, "name", "preview", "file", "path", "mediaType", 12345);
    }


}