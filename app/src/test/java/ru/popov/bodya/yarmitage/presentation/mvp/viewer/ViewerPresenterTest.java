package ru.popov.bodya.yarmitage.presentation.mvp.viewer;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;

/**
 * @author popovbodya
 */
@RunWith(MockitoJUnitRunner.class)
public class ViewerPresenterTest {

    @Mock
    private ViewerView mViewerView;
    @Mock
    private ViewerView$$State mViewerViewState;

    private ViewerPresenter mViewerPresenter;


    @Test
    public void testOnFirstViewAttach() {

        final int size = 10;
        final List<String> urisList = createUriList(size);
        final int position = 5;
        mViewerPresenter = new ViewerPresenter(urisList, position);
        mViewerPresenter.setViewState(mViewerViewState);

        mViewerPresenter.attachView(mViewerView);

        verify(mViewerViewState).setTitleText(eq(String.format("%1$d из %2$d", position + 1, size)));
        verify(mViewerViewState).setUriList(eq(urisList));
        verify(mViewerViewState).selectPage(eq(position));
    }


    @Test
    public void testOnPageSelected() {

        final int size = 10;
        final int position = 5;
        final int selectedPosition = 4;
        final List<String> urisList = createUriList(size);
        mViewerPresenter = new ViewerPresenter(urisList, position);
        mViewerPresenter.setViewState(mViewerViewState);

        mViewerPresenter.onPageSelected(selectedPosition);
        verify(mViewerViewState).setTitleText(eq(String.format("%1$d из %2$d", selectedPosition + 1, size)));
        verifyNoMoreInteractions(mViewerViewState);
    }

    private List<String> createUriList(int size) {
        final List<String> urisList = new ArrayList<>();
        for (int i = 0; i < size; i++) {
            urisList.add("uri" + i);
        }
        return urisList;
    }

}