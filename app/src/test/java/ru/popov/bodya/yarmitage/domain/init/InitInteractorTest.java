package ru.popov.bodya.yarmitage.domain.init;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import io.reactivex.Single;

import static org.mockito.Mockito.when;

/**
 * @author popovbodya
 */
@RunWith(MockitoJUnitRunner.class)
public class InitInteractorTest {

    @Mock
    private InitRepository mInitRepository;

    private InitInteractor mInitInteractor;

    @Before
    public void setUp() {
        mInitInteractor = new InitInteractor(mInitRepository);
    }

    @Test
    public void testGetToken() {

        final String token = "qwerty123";
        when(mInitRepository.getToken()).thenReturn(Single.just(token));

        mInitInteractor
                .getToken()
                .test()
                .assertResult(token);
    }

    @Test(expected = RuntimeException.class)
    public void testGetTokenWithError() {

        when(mInitRepository.getToken()).thenThrow(new RuntimeException());

        mInitInteractor
                .getToken()
                .test()
                .assertError(RuntimeException.class)
                .assertNoValues();
    }
}