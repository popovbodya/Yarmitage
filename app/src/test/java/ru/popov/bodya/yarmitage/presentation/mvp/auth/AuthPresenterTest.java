package ru.popov.bodya.yarmitage.presentation.mvp.auth;

import android.content.Intent;

import com.yandex.authsdk.YandexAuthException;
import com.yandex.authsdk.YandexAuthToken;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import io.reactivex.Completable;
import io.reactivex.Single;
import ru.popov.bodya.yarmitage.app.rx.RxSchedulers;
import ru.popov.bodya.yarmitage.app.rx.RxSchedulersStub;
import ru.popov.bodya.yarmitage.app.rx.RxSchedulersTransformer;
import ru.popov.bodya.yarmitage.app.rx.RxSchedulersTransformerImpl;
import ru.popov.bodya.yarmitage.domain.auth.AuthInteractor;
import ru.popov.bodya.yarmitage.presentation.ui.global.Screens;
import ru.terrakok.cicerone.Router;

import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;

/**
 * @author popovbodya
 */
@RunWith(MockitoJUnitRunner.class)
public class AuthPresenterTest {

    @Mock
    private Router mRouter;
    @Mock
    private AuthInteractor mAuthInteractor;
    @Mock
    private AuthView mAuthView;
    @Mock
    private AuthView$$State mAuthViewState;

    private AuthPresenter mAuthPresenter;
    private RxSchedulers mRxSchedulers;

    @Before
    public void setUp() {
        mRxSchedulers = spy(new RxSchedulersStub());
        final RxSchedulersTransformer rxSchedulersTransformer = new RxSchedulersTransformerImpl(mRxSchedulers);
        mAuthPresenter = new AuthPresenter(mRouter, mAuthInteractor, rxSchedulersTransformer);
        mAuthPresenter.setViewState(mAuthViewState);
    }

    @Test
    public void testOnFirstViewAttach() {

        final Intent testIntent = new Intent();
        Single<Intent> testSingle = Single.just(testIntent);
        when(mAuthInteractor.getAuthIntent()).thenReturn(testSingle);

        mAuthPresenter.attachView(mAuthView);

        verify(mAuthInteractor).getAuthIntent();
        verifyNoMoreInteractions(mAuthInteractor);

        verify(mRxSchedulers).getIOScheduler();
        verify(mRxSchedulers).getMainThreadScheduler();
        verifyNoMoreInteractions(mRxSchedulers);

        verify(mRouter).navigateTo(eq(Screens.YANDEX_LOGIN_SCREEN), eq(testIntent));
        verifyNoMoreInteractions(mRouter);
    }

    @Test
    public void testOnActivityResultNotOk() {

        final Intent testIntent = new Intent();
        final int activityResult = 0;

        mAuthPresenter.onActivityResult(activityResult, testIntent);

        verify(mAuthViewState).showLoading(eq(false));
        verifyNoMoreInteractions(mAuthViewState);
    }

    @Test
    public void testOnActivityResultWithOk() throws YandexAuthException {

        final Intent testIntent = new Intent();
        final String token = "qwerty";
        final long expiresIn = 12345678901L;
        final int activityResult = -1;
        final YandexAuthToken yandexAuthToken = new YandexAuthToken(token, expiresIn);

        Single<YandexAuthToken> authTokenSingle = Single.just(yandexAuthToken);
        when(mAuthInteractor.extractToken(activityResult, testIntent)).thenReturn(authTokenSingle);
        when(mAuthInteractor.saveToken(yandexAuthToken)).thenReturn(Completable.complete());

        mAuthPresenter.onActivityResult(activityResult, testIntent);

        verify(mAuthInteractor).extractToken(eq(activityResult), eq(testIntent));
        verify(mAuthInteractor).saveToken(eq(yandexAuthToken));
        verifyNoMoreInteractions(mAuthInteractor);

        verify(mRxSchedulers, times(2)).getIOScheduler();
        verify(mRxSchedulers, times(2)).getMainThreadScheduler();
        verifyNoMoreInteractions(mRxSchedulers);

        verify(mAuthViewState).showLoading(eq(true));

        verify(mRouter).newRootScreen(eq(Screens.FEED_SCREEN), eq(token));

    }
}