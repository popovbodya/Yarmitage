package ru.popov.bodya.yarmitage.data.repositories;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.Single;
import ru.popov.bodya.yarmitage.data.database.converter.ResourceEntityConverter;
import ru.popov.bodya.yarmitage.data.database.dao.ResourcesDao;
import ru.popov.bodya.yarmitage.data.database.entities.ResourceEntity;
import ru.popov.bodya.yarmitage.data.network.api.YandexDiskApi;
import ru.popov.bodya.yarmitage.data.network.beans.ResourceBean;
import ru.popov.bodya.yarmitage.data.network.beans.ResourceListBean;
import ru.popov.bodya.yarmitage.data.network.converter.ResourceConverter;
import ru.popov.bodya.yarmitage.domain.feed.FeedRepository;
import ru.popov.bodya.yarmitage.domain.global.models.Resource;

import static org.mockito.Matchers.anyMapOf;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.verifyZeroInteractions;
import static org.mockito.Mockito.when;

/**
 * @author popovbodya
 */
@RunWith(MockitoJUnitRunner.class)
public class FeedRepositoryImplTest {

    @Mock
    private YandexDiskApi mYandexDiskApi;
    @Mock
    private ResourcesDao mResourcesDao;
    @Mock
    private ResourceEntityConverter mResourceEntityConverter;
    @Mock
    private ResourceConverter mResourceConverter;

    private FeedRepository mFeedRepository;

    @Before
    public void setUp()  {
        mFeedRepository = new FeedRepositoryImpl(mYandexDiskApi, mResourcesDao, mResourceEntityConverter, mResourceConverter);
    }

    @Test
    public void testGetFilesFromCache() {

        final int size = 20;
        final List<ResourceEntity> resourceEntityList = createResourceEntityList(size);
        final List<Resource> resourceList = createResourceList(size);

        when(mResourcesDao.getResources()).thenReturn(resourceEntityList);
        when(mResourceEntityConverter.reverseList(resourceEntityList)).thenReturn(resourceList);

        //noinspection unchecked
        mFeedRepository
                .getFilesFromCache()
                .test()
                .assertResult(resourceList);

        verify(mResourcesDao).getResources();
        verifyNoMoreInteractions(mResourcesDao);
        verify(mResourceEntityConverter).reverseList(eq(resourceEntityList));
        verifyNoMoreInteractions(mResourceEntityConverter);
    }

    @Test
    public void testGetFilesFromNetworkWithSaveInCache() {

        final int size = 20;
        final int offset = 40;
        final int limit = 20;
        List<ResourceBean> resourceBeanList = createResourceBeanList(size);
        ResourceListBean resourceListBean = new ResourceListBean(resourceBeanList);
        List<Resource> resourceList = createResourceList(size);
        List<ResourceEntity> resourceEntityList = createResourceEntityList(size);

        when(mYandexDiskApi.getFiles(anyMapOf(String.class, String.class))).thenReturn(Single.just(resourceListBean));
        when(mResourceConverter.convertList(resourceListBean)).thenReturn(resourceList);
        when(mResourceEntityConverter.convertList(resourceList)).thenReturn(resourceEntityList);

        //noinspection unchecked
        mFeedRepository
                .getFilesFromNetwork(offset, limit, true)
                .test()
                .assertResult(resourceList);

        verify(mYandexDiskApi).getFiles(anyMapOf(String.class, String.class));
        verifyNoMoreInteractions(mYandexDiskApi);

        verify(mResourceConverter).convertList(eq(resourceListBean));
        verifyNoMoreInteractions(mResourceConverter);

        verify(mResourceEntityConverter).convertList(resourceList);
        verifyNoMoreInteractions(mResourceEntityConverter);

        verify(mResourcesDao).deleteAllData();
        verify(mResourcesDao).insertResourceList(resourceEntityList);
        verifyNoMoreInteractions(mResourcesDao);
    }

    @Test
    public void testGetFilesFromNetworkWithNoSaveInCache() {
        final int size = 20;
        final int offset = 40;
        final int limit = 20;
        List<ResourceBean> resourceBeanList = createResourceBeanList(size);
        ResourceListBean resourceListBean = new ResourceListBean(resourceBeanList);
        List<Resource> resourceList = createResourceList(size);
        List<ResourceEntity> resourceEntityList = createResourceEntityList(size);

        when(mYandexDiskApi.getFiles(anyMapOf(String.class, String.class))).thenReturn(Single.just(resourceListBean));
        when(mResourceConverter.convertList(resourceListBean)).thenReturn(resourceList);

        //noinspection unchecked
        mFeedRepository
                .getFilesFromNetwork(offset, limit, false)
                .test()
                .assertResult(resourceList);

        verify(mYandexDiskApi).getFiles(anyMapOf(String.class, String.class));
        verifyNoMoreInteractions(mYandexDiskApi);

        verify(mResourceConverter).convertList(eq(resourceListBean));
        verifyNoMoreInteractions(mResourceConverter);

        verifyZeroInteractions(mResourceEntityConverter);
        verifyZeroInteractions(mResourcesDao);
    }

    private List<ResourceBean> createResourceBeanList(int size) {
        final List<ResourceBean> resourceList = new ArrayList<>();
        for (int i = 0; i < size; i++) {
            resourceList.add(createResourceBean(i));
        }
        return resourceList;
    }

    private List<ResourceEntity> createResourceEntityList(int size) {
        final List<ResourceEntity> resourceList = new ArrayList<>();
        for (int i = 0; i < size; i++) {
            resourceList.add(createResourceEntity(i));
        }
        return resourceList;
    }

    private List<Resource> createResourceList(int size) {
        final List<Resource> resourceList = new ArrayList<>();
        for (int i = 0; i < size; i++) {
            resourceList.add(createResource(i));
        }
        return resourceList;
    }

    private ResourceBean createResourceBean(int id) {
        return new ResourceBean("resourceId" + id, "name", "preview", "file", "path", "mediaType", 12345);
    }

    private ResourceEntity createResourceEntity(int id) {
        return new ResourceEntity("resourceId" + id, "name", "preview", "file", "path", "mediaType", 12345);
    }

    private Resource createResource(int id) {
        return new Resource("resourceId" + id, "name", "preview", "file", "path", "mediaType", 12345);
    }
}