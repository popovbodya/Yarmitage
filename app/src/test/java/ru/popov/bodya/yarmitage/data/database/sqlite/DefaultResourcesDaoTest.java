package ru.popov.bodya.yarmitage.data.database.sqlite;

import android.content.Context;
import android.support.annotation.NonNull;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.RuntimeEnvironment;

import java.util.ArrayList;
import java.util.List;

import ru.popov.bodya.yarmitage.data.database.dao.ResourcesDao;
import ru.popov.bodya.yarmitage.data.database.entities.ResourceEntity;

import static org.hamcrest.CoreMatchers.everyItem;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.Matchers.isIn;
import static org.junit.Assert.*;

/**
 * @author popovbodya
 */
@RunWith(RobolectricTestRunner.class)
public class DefaultResourcesDaoTest {

    private static final String RESOURCE_ID = "resourceId";

    private ResourcesDao mResourcesDao;

    @Before
    public void setUp() {
        Context context = RuntimeEnvironment.application.getApplicationContext();
        ResourcesSQLiteOpenHelper helper = new ResourcesSQLiteOpenHelper(context);
        mResourcesDao = new DefaultResourcesDao(helper);
    }

    @Test
    public void testInsertResource() {
        final ResourceEntity resourceEntity = createResource();
        final long rowId = mResourcesDao.insertResource(resourceEntity);
        assertThat(rowId, is(not(-1L)));
    }

    @Test
    public void testGetResource() {

        final ResourceEntity expectedEntity = createResource();
        final long rowId = mResourcesDao.insertResource(expectedEntity);
        assertThat(rowId, is(not(-1L)));

        final ResourceEntity actualEntity = mResourcesDao.getResourceById(RESOURCE_ID);
        assertThat(actualEntity, is(expectedEntity));
    }

    @Test
    public void testInsertResourceList() {

        // resource creation
        final int size = 10;
        final List<ResourceEntity> expectedEntityList = new ArrayList<>();
        for (int i = 0; i < size; i++) {
            final ResourceEntity resourceWithId = createResourceWithId(RESOURCE_ID + i);
            expectedEntityList.add(resourceWithId);
        }
        mResourcesDao.insertResourceList(expectedEntityList);

        final List<ResourceEntity> actualEntityList = mResourcesDao.getResources();
        assertThat(actualEntityList.size(), is(expectedEntityList.size()));
        assertThat(actualEntityList, everyItem(isIn(expectedEntityList)));
    }

    @Test
    public void testGetResourceList() {

        final ResourceEntity firstResource = createResourceWithId(RESOURCE_ID + "1");
        final ResourceEntity secondResource = createResourceWithId(RESOURCE_ID + "2");

        final List<ResourceEntity> expectedEntityList = new ArrayList<>();
        expectedEntityList.add(firstResource);
        expectedEntityList.add(secondResource);

        mResourcesDao.insertResource(firstResource);
        mResourcesDao.insertResource(secondResource);

        final List<ResourceEntity> actualEntityList = mResourcesDao.getResources();
        assertThat(actualEntityList.size(), is(expectedEntityList.size()));
        assertThat(actualEntityList, everyItem(isIn(expectedEntityList)));
    }

    @Test
    public void testDeleteResource() {

        // resource creation
        final int size = 10;
        final List<ResourceEntity> expectedEntityList = new ArrayList<>();
        for (int i = 0; i < size; i++) {
            final ResourceEntity resourceWithId = createResourceWithId(RESOURCE_ID + i);
            expectedEntityList.add(resourceWithId);
        }

        mResourcesDao.insertResourceList(expectedEntityList);

        final List<ResourceEntity> actualEntityList = mResourcesDao.getResources();
        assertThat(actualEntityList.size(), is(size));
        assertThat(actualEntityList, everyItem(isIn(expectedEntityList)));

        // test delete
        final int randomIndexToDelete = (int) (Math.random() * size);
        final ResourceEntity resourceToDelete = expectedEntityList.remove(randomIndexToDelete);
        mResourcesDao.deleteResource(resourceToDelete);
        final List<ResourceEntity> resourceListAfterDelete = mResourcesDao.getResources();

        assertThat(resourceListAfterDelete.size(), is(expectedEntityList.size()));
        assertThat(resourceListAfterDelete, everyItem(isIn(expectedEntityList)));
    }

    @Test
    public void testDeleteAllData() {

        // resource creation
        final long size = 10;
        final List<ResourceEntity> stubList = new ArrayList<>();
        for (int i = 0; i < size; i++) {
            final ResourceEntity resourceWithId = createResourceWithId(RESOURCE_ID + i);
            stubList.add(resourceWithId);
        }

        mResourcesDao.insertResourceList(stubList);

        final List<ResourceEntity> expectedResourceList = new ArrayList<>();
        final long actualDeletedRowsCount = mResourcesDao.deleteAllData();
        final List<ResourceEntity> actualResourceList = mResourcesDao.getResources();

        assertThat(actualDeletedRowsCount, is(size));
        assertThat(actualResourceList.size(), is(expectedResourceList.size()));
        assertThat(actualResourceList, everyItem(isIn(expectedResourceList)));
    }


    @NonNull
    private ResourceEntity createResource() {
        return new ResourceEntity(RESOURCE_ID, "name", "preview", "file", "path", "mediaType", 0);
    }

    @NonNull
    private ResourceEntity createResourceWithId(String resourceId) {
        return new ResourceEntity(resourceId, "name", "preview", "file", "path", "mediaType", 0);
    }

}