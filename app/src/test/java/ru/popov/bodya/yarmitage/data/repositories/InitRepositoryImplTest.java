package ru.popov.bodya.yarmitage.data.repositories;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import ru.popov.bodya.yarmitage.data.database.preferences.SharedPreferencesWrapper;
import ru.popov.bodya.yarmitage.domain.init.InitRepository;

import static org.mockito.Mockito.when;

/**
 * @author popovbodya
 */
@RunWith(MockitoJUnitRunner.class)
public class InitRepositoryImplTest {

    @Mock
    private SharedPreferencesWrapper mSharedPreferencesWrapper;

    private InitRepository mInitRepository;

    @Before
    public void setUp() {
        mInitRepository = new InitRepositoryImpl(mSharedPreferencesWrapper);
    }

    @Test
    public void testGetToken() {

        final String token = "qwerty12345";
        when(mSharedPreferencesWrapper.getToken()).thenReturn(token);

        mInitRepository
                .getToken()
                .test()
                .assertResult(token);
    }
}