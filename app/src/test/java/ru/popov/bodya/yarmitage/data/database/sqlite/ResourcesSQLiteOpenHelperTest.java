package ru.popov.bodya.yarmitage.data.database.sqlite;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.RuntimeEnvironment;

/**
 * @author popovbodya
 */
@RunWith(RobolectricTestRunner.class)
public class ResourcesSQLiteOpenHelperTest {


    private ResourcesSQLiteOpenHelper mHelper;

    @Before
    public void setUp() {
        Context context = RuntimeEnvironment.application.getApplicationContext();
        mHelper = new ResourcesSQLiteOpenHelper(context);
    }

    @Test
    public void testTablesExists() {
        try (SQLiteDatabase db = mHelper.getWritableDatabase()) {
            DatabaseTestUtils.assertTableExists(db, YarmitageDBContract.Resources.TABLE_NAME, YarmitageDBContract.Resources.ALL_COLUMNS);
        }
    }

    @After
    public void tearDown() {
        Context context = RuntimeEnvironment.application.getApplicationContext();
        context.deleteDatabase(ResourcesSQLiteOpenHelper.DB_NAME);
    }

}