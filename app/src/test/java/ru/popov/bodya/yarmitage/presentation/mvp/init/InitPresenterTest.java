package ru.popov.bodya.yarmitage.presentation.mvp.init;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import io.reactivex.Single;
import ru.popov.bodya.yarmitage.app.rx.RxSchedulers;
import ru.popov.bodya.yarmitage.app.rx.RxSchedulersStub;
import ru.popov.bodya.yarmitage.app.rx.RxSchedulersTransformer;
import ru.popov.bodya.yarmitage.app.rx.RxSchedulersTransformerImpl;
import ru.popov.bodya.yarmitage.domain.init.InitInteractor;
import ru.popov.bodya.yarmitage.presentation.ui.global.Screens;
import ru.terrakok.cicerone.Router;

import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;

/**
 * @author popovbodya
 */
@RunWith(MockitoJUnitRunner.class)
public class InitPresenterTest {

    @Mock
    private Router mRouter;
    @Mock
    private InitInteractor mInitInteractor;
    @Mock
    private InitView mInitView;
    @Mock
    private InitView$$State mInitViewState;

    private RxSchedulers mRxSchedulers;
    private InitPresenter mInitPresenter;

    @Before
    public void setUp() {
        mRxSchedulers = spy(new RxSchedulersStub());
        final RxSchedulersTransformer rxSchedulersTransformer = new RxSchedulersTransformerImpl(mRxSchedulers);
        mInitPresenter = new InitPresenter(mRouter, mInitInteractor, rxSchedulersTransformer);
        mInitPresenter.setViewState(mInitViewState);
    }

    @Test
    public void testOnFirstViewAttachWithValidToken() {

        final String token = "qwerty";
        when(mInitInteractor.getToken()).thenReturn(Single.just(token));

        mInitPresenter.attachView(mInitView);

        verify(mInitInteractor).getToken();
        verifyNoMoreInteractions(mInitInteractor);

        verify(mRxSchedulers).getIOScheduler();
        verify(mRxSchedulers).getMainThreadScheduler();
        verifyNoMoreInteractions(mRxSchedulers);

        verify(mRouter).newRootScreen(eq(Screens.FEED_SCREEN), eq(token));
        verifyNoMoreInteractions(mRouter);
    }

    @Test
    public void testOnFirstViewAttachWithNonValidToken() {

        final String token = "";
        when(mInitInteractor.getToken()).thenReturn(Single.just(token));

        mInitPresenter.attachView(mInitView);

        verify(mInitInteractor).getToken();
        verifyNoMoreInteractions(mInitInteractor);

        verify(mRxSchedulers).getIOScheduler();
        verify(mRxSchedulers).getMainThreadScheduler();
        verifyNoMoreInteractions(mRxSchedulers);

        verify(mRouter).newRootScreen(eq(Screens.AUTH_SCREEN));
        verifyNoMoreInteractions(mRouter);
    }

}