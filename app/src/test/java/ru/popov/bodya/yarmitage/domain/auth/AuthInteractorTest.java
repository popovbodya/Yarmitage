package ru.popov.bodya.yarmitage.domain.auth;

import android.content.Intent;

import com.yandex.authsdk.YandexAuthException;
import com.yandex.authsdk.YandexAuthToken;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import io.reactivex.Completable;
import io.reactivex.Single;

import static org.mockito.Mockito.when;

/**
 * @author popovbodya
 */
@RunWith(MockitoJUnitRunner.class)
public class AuthInteractorTest {

    @Mock
    private AuthRepository mAuthRepository;

    private AuthInteractor mAuthInteractor;

    @Before
    public void setUp() throws Exception {
        mAuthInteractor = new AuthInteractor(mAuthRepository);
    }

    @Test
    public void testGetAuthIntent() {

        final Intent intent = new Intent();
        Single<Intent> intentSingle = Single.just(intent);
        when(mAuthRepository.getAuthIntent()).thenReturn(intentSingle);

        mAuthInteractor
                .getAuthIntent()
                .test()
                .assertResult(intent);
    }

    @Test
    public void testExtractToken() throws YandexAuthException {

        final String token = "qwerty123";
        final Intent intent = new Intent();
        final int result = -1;
        final YandexAuthToken yandexAuthToken = new YandexAuthToken(token, 123456);

        when(mAuthRepository.extractToken(result, intent)).thenReturn(Single.just(yandexAuthToken));

        mAuthInteractor
                .extractToken(result, intent)
                .test()
                .assertResult(yandexAuthToken);
    }

    @Test
    public void testSaveToken() {

        final YandexAuthToken yandexAuthToken = new YandexAuthToken("qwerty123", 123456);
        when(mAuthRepository.saveToken(yandexAuthToken)).thenReturn(Completable.complete());

        mAuthInteractor
                .saveToken(yandexAuthToken)
                .test()
                .assertComplete()
                .assertNoErrors();
    }
}