package ru.popov.bodya.yarmitage.presentation.mvp.global.pagination;

import java.util.List;

/**
 * @author popovbodya
 */

public interface State<T> {

    default void refresh() {
    }

    default void loadNewPage() {
    }

    default void release() {
    }

    default void newData(List<T> data) {
    }

    default void fail(Throwable error) {
    }

}
