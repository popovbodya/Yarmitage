package ru.popov.bodya.yarmitage.data.network.api.constants;

import android.support.annotation.StringDef;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

import static ru.popov.bodya.yarmitage.data.network.api.constants.DiskApiConstants.BASE_DISK_API_URL;

/**
 * @author popovbodya
 */

@Retention(RetentionPolicy.SOURCE)
@StringDef({BASE_DISK_API_URL})
public @interface DiskApiConstants {

    String BASE_DISK_API_URL = "https://cloud-api.yandex.net/";
}
