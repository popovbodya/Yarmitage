package ru.popov.bodya.yarmitage.di.viewer;

import android.content.Context;

import java.util.List;

import ru.popov.bodya.yarmitage.di.global.AppComponent;
import ru.popov.bodya.yarmitage.di.global.ComponentUtils;

/**
 * @author popovbodya
 */
public class ViewerInjector {

    private static ViewerSubComponent sViewerSubComponent = null;

    private ViewerInjector() {
        throw new UnsupportedOperationException("No instances");
    }

    public static ViewerSubComponent getViewerSubComponent(Context context, List<String> urisList, int targetPosition) {
        if (sViewerSubComponent == null) {
            sViewerSubComponent = ComponentUtils.getComponent(context, AppComponent.class)
                    .viewerSubComponentBuilder()
                    .urisList(urisList)
                    .targetPosition(targetPosition)
                    .build();
        }
        return sViewerSubComponent;
    }

    public static void clearViewerSubComponent() {
        sViewerSubComponent = null;
    }

}