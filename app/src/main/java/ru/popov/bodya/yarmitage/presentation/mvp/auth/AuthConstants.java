package ru.popov.bodya.yarmitage.presentation.mvp.auth;

import android.support.annotation.IntDef;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

import static ru.popov.bodya.yarmitage.presentation.mvp.auth.AuthConstants.REQUEST_LOGIN_SDK;

/**
 * @author popovbodya
 */

@Retention(RetentionPolicy.SOURCE)
@IntDef({REQUEST_LOGIN_SDK})
public @interface AuthConstants {

    /**
     * Код логина от Yandex Sdk
     */
    int REQUEST_LOGIN_SDK = 1;
}
