package ru.popov.bodya.yarmitage.di.init.modules;

import dagger.Module;
import dagger.Provides;
import ru.popov.bodya.yarmitage.di.init.InitScope;
import ru.popov.bodya.yarmitage.domain.init.InitInteractor;
import ru.popov.bodya.yarmitage.domain.init.InitRepository;

/**
 * @author popovbodya
 */
@Module
public class InitInteractorModule {

    @InitScope
    @Provides
    InitInteractor provideInitInteractor(InitRepository initRepository) {
        return new InitInteractor(initRepository);
    }

}
