package ru.popov.bodya.yarmitage.di.init.modules;

import dagger.Module;
import dagger.Provides;
import ru.popov.bodya.yarmitage.app.rx.RxSchedulersTransformer;
import ru.popov.bodya.yarmitage.di.init.InitScope;
import ru.popov.bodya.yarmitage.domain.init.InitInteractor;
import ru.popov.bodya.yarmitage.presentation.mvp.init.InitPresenter;
import ru.terrakok.cicerone.Router;

/**
 * @author popovbodya
 */
@Module
public class InitPresenterModule {

    @InitScope
    @Provides
    InitPresenter provideInitPresenter(Router router, InitInteractor initInteractor, RxSchedulersTransformer rxSchedulersTransformer) {
        return new InitPresenter(router, initInteractor, rxSchedulersTransformer);
    }

}
