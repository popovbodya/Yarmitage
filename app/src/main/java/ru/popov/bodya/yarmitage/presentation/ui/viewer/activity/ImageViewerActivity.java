package ru.popov.bodya.yarmitage.presentation.ui.viewer.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;

import com.arellomobile.mvp.presenter.InjectPresenter;
import com.arellomobile.mvp.presenter.ProvidePresenter;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import ru.popov.bodya.yarmitage.R;
import ru.popov.bodya.yarmitage.di.viewer.ViewerInjector;
import ru.popov.bodya.yarmitage.presentation.mvp.viewer.ViewerPresenter;
import ru.popov.bodya.yarmitage.presentation.mvp.viewer.ViewerView;
import ru.popov.bodya.yarmitage.presentation.ui.global.BaseCoreActivity;
import ru.popov.bodya.yarmitage.presentation.ui.global.OnPageChangeAdapter;
import ru.popov.bodya.yarmitage.presentation.ui.viewer.adapter.ImageViewerPagerAdapter;

/**
 * @author popovbodya
 */

public class ImageViewerActivity extends BaseCoreActivity implements ViewerView {

    public static final String TARGET_POSITION_ARG = "target_position_arg";
    public static final String IMAGES_URIS_ARG = "images_uris_arg";

    @Inject
    @InjectPresenter
    ViewerPresenter mViewerPresenter;

    private Toolbar mToolbar;
    private ViewPager mViewPager;
    private ImageViewerPagerAdapter mPagerAdapter;

    public static Intent newIntent(Context context, int targetPosition, List<String> imagesUris) {
        Intent intent = new Intent(context, ImageViewerActivity.class);
        intent.putExtra(TARGET_POSITION_ARG, targetPosition);
        intent.putStringArrayListExtra(IMAGES_URIS_ARG, new ArrayList<>(imagesUris));
        return intent;
    }

    @ProvidePresenter
    ViewerPresenter provideViewerPresenter() {
        return mViewerPresenter;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        injectDependencies();

        super.onCreate(savedInstanceState);
        setContentView(R.layout.viewer_activity_layout);

        initViews();
        initToolbar();
        initPager();
    }

    @Override
    protected void onDestroy() {
        if (isFinishing()) {
            ViewerInjector.clearViewerSubComponent();
        }
        super.onDestroy();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        final int itemId = item.getItemId();
        if (itemId == android.R.id.home) {
            onBackPressed();
            return true;
        } else {
            return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void setTitleText(String title) {
        setTitle(title);
    }

    @Override
    public void selectPage(int position) {
        mViewPager.setCurrentItem(position);
    }

    @Override
    public void setUriList(List<String> urisList) {
        mPagerAdapter.setUrisList(urisList);
    }

    private void initViews() {
        mToolbar = findViewById(R.id.toolbar);
        mViewPager = findViewById(R.id.view_pager);
    }

    private void injectDependencies() {
        final ArrayList<String> urisList = getIntent().getStringArrayListExtra(IMAGES_URIS_ARG);
        final int targetPosition = getIntent().getIntExtra(TARGET_POSITION_ARG, 0);
        ViewerInjector
                .getViewerSubComponent(this, urisList, targetPosition)
                .inject(this);
    }

    private void initToolbar() {
        setSupportActionBar(mToolbar);
        final ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
        }
    }

    private void initPager() {
        mPagerAdapter = new ImageViewerPagerAdapter(getSupportFragmentManager());
        mViewPager.setAdapter(mPagerAdapter);
        mViewPager.addOnPageChangeListener(new PageSelectedListener());

    }

    private class PageSelectedListener extends OnPageChangeAdapter {
        @Override
        public void onPageSelected(int position) {
            mViewerPresenter.onPageSelected(position);
        }
    }
}
