package ru.popov.bodya.yarmitage.di.global.modules;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import ru.terrakok.cicerone.Cicerone;
import ru.terrakok.cicerone.NavigatorHolder;
import ru.terrakok.cicerone.Router;

/**
 * @author popovbodya
 */
@Module
public class NavigationModule {

    @Provides
    @Singleton
    Cicerone<? extends Router> provideCicerone() {
        return Cicerone.create();
    }

    @Provides
    @Singleton
    Router provideRouter(Cicerone<? extends Router> cicerone) {
        return cicerone.getRouter();
    }

    @Provides
    @Singleton
    NavigatorHolder provideNavigationHolder(Cicerone<? extends Router> cicerone) {
        return cicerone.getNavigatorHolder();
    }

}
