package ru.popov.bodya.yarmitage.di.global;

import android.content.Context;

/**
 * @author popovbodya
 */

public class ComponentUtils {

    private ComponentUtils() {
        throw new IllegalStateException("No instances");
    }

    @SuppressWarnings("unchecked")
    public static <C> C getComponent(Context context, Class<C> componentType) {
        return componentType.cast(((IHasComponent<C>) context.getApplicationContext()).getComponent());
    }

}
