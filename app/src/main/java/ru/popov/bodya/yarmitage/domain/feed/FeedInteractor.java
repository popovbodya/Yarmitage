package ru.popov.bodya.yarmitage.domain.feed;

import android.support.v7.util.DiffUtil;

import java.util.List;

import io.reactivex.Flowable;
import io.reactivex.Single;
import ru.popov.bodya.yarmitage.app.rx.RxSchedulers;
import ru.popov.bodya.yarmitage.domain.global.models.Resource;

/**
 * @author popovbodya
 */

public class FeedInteractor {

    private static final int INITIAL_OFFSET = 0;
    private static final int DEFAULT_LIMIT = 20;

    private final FeedRepository mFeedRepository;
    private final RxSchedulers mRxSchedulers;

    public FeedInteractor(FeedRepository feedRepository, RxSchedulers rxSchedulers) {
        mFeedRepository = feedRepository;
        mRxSchedulers = rxSchedulers;
    }

    public Single<List<Resource>> getImages(int page) {
        return mFeedRepository.getFilesFromNetwork(DEFAULT_LIMIT * page, DEFAULT_LIMIT, false);
    }

    public Flowable<List<Resource>> initialFetchDataFromAllSources() {
        return Single
                .mergeDelayError(
                        mFeedRepository.getFilesFromNetwork(INITIAL_OFFSET, DEFAULT_LIMIT, true).subscribeOn(mRxSchedulers.getIOScheduler()),
                        mFeedRepository.getFilesFromCache().subscribeOn(mRxSchedulers.getIOScheduler()))
                .filter(resources -> !resources.isEmpty());
    }

    public Single<DiffUtil.DiffResult> evaluateDiff(DiffUtil.Callback callback) {
        return Single.fromCallable(() -> DiffUtil.calculateDiff(callback, true));
    }
}


