package ru.popov.bodya.yarmitage.presentation.ui.viewer.fragment;

import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;

import ru.popov.bodya.yarmitage.R;
import ru.popov.bodya.yarmitage.presentation.ui.global.BaseCoreFragment;

/**
 * @author popovbodya
 */

public class ImageViewerPageFragment extends BaseCoreFragment {

    private static final String ARG_IMAGE_URI = "arg_image_uri";

    private ProgressBar mProgressBar;
    private ImageView mImageView;
    private ViewGroup mZeroLayout;
    private String mUri;

    public static ImageViewerPageFragment newInstance(String imageUri) {
        Bundle args = new Bundle();
        args.putString(ARG_IMAGE_URI, imageUri);

        ImageViewerPageFragment fragment = new ImageViewerPageFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.viewer_fragment_layout, container, false);
        mProgressBar = view.findViewById(R.id.progress_bar);
        mImageView = view.findViewById(R.id.photo_view);
        mZeroLayout = view.findViewById(R.id.zero_layout);
        Button loadButton = view.findViewById(R.id.load_again_button);
        loadButton.setOnClickListener(v -> {
            showLoading(true);
            showError(false);
            tryLoadImage();
        });
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        final Bundle arguments = getArguments();
        if (arguments != null && (mUri = arguments.getString(ARG_IMAGE_URI)) != null) {
            tryLoadImage();
        }
    }

    private void tryLoadImage() {
        Glide
                .with(this)
                .load(mUri)
                .listener(mRequestListener)
                .into(mImageView);
    }

    private void showLoading(boolean show) {
        mProgressBar.setVisibility(show ? View.VISIBLE : View.GONE);
    }

    private void showError(boolean show) {
        mZeroLayout.setVisibility(show ? View.VISIBLE : View.GONE);
    }

    private RequestListener<Drawable> mRequestListener = new RequestListener<Drawable>() {
        @Override
        public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
            showError(true);
            showLoading(false);
            return true;
        }

        @Override
        public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
            showLoading(false);
            return false;
        }
    };
}
