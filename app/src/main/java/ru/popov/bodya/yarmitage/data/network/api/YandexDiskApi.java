package ru.popov.bodya.yarmitage.data.network.api;

import java.util.Map;

import io.reactivex.Single;
import retrofit2.http.GET;
import retrofit2.http.QueryMap;
import ru.popov.bodya.yarmitage.data.network.beans.ResourceListBean;

/**
 * @author popovbodya
 */

public interface YandexDiskApi {

    @GET("v1/disk/resources/files")
    Single<ResourceListBean> getFiles(@QueryMap Map<String, String> params);

}
