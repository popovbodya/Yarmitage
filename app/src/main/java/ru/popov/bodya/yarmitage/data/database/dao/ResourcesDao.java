package ru.popov.bodya.yarmitage.data.database.dao;


import java.util.List;

import ru.popov.bodya.yarmitage.data.database.entities.ResourceEntity;

public interface ResourcesDao {

    long insertResource(ResourceEntity resourceEntity);

    long insertResourceList(List<ResourceEntity> resourceEntityList);

    ResourceEntity getResourceById(String resourceId);

    List<ResourceEntity> getResources();

    long deleteResource(ResourceEntity resourceEntity);

    long deleteAllData();

}
