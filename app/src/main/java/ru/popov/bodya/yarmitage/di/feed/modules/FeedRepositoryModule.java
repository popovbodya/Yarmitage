package ru.popov.bodya.yarmitage.di.feed.modules;

import android.content.Context;

import dagger.Module;
import dagger.Provides;
import ru.popov.bodya.yarmitage.data.database.converter.ResourceEntityConverter;
import ru.popov.bodya.yarmitage.data.database.dao.ResourcesDao;
import ru.popov.bodya.yarmitage.data.database.sqlite.DefaultResourcesDao;
import ru.popov.bodya.yarmitage.data.database.sqlite.ResourcesSQLiteOpenHelper;
import ru.popov.bodya.yarmitage.data.network.api.YandexDiskApi;
import ru.popov.bodya.yarmitage.data.network.converter.ResourceConverter;
import ru.popov.bodya.yarmitage.data.repositories.FeedRepositoryImpl;
import ru.popov.bodya.yarmitage.di.feed.FeedScope;
import ru.popov.bodya.yarmitage.di.global.ApplicationContext;
import ru.popov.bodya.yarmitage.domain.feed.FeedRepository;

/**
 * @author popovbodya
 */
@Module
public class FeedRepositoryModule {

    @Provides
    @FeedScope
    FeedRepository provideFeedRepository(YandexDiskApi yandexDiskApi, ResourcesDao resourcesDao, ResourceEntityConverter resourceEntityConverter, ResourceConverter resourceConverter) {
        return new FeedRepositoryImpl(yandexDiskApi, resourcesDao, resourceEntityConverter, resourceConverter);
    }

    @Provides
    @FeedScope
    ResourcesDao provideResourcesDao(ResourcesSQLiteOpenHelper sqLiteOpenHelper) {
        return new DefaultResourcesDao(sqLiteOpenHelper);
    }

    @Provides
    @FeedScope
    ResourcesSQLiteOpenHelper provideResourcesSQLiteOpenHelper(@ApplicationContext Context context) {
        return new ResourcesSQLiteOpenHelper(context);
    }

    @Provides
    @FeedScope
    ResourceConverter provideResourceConverter() {
        return new ResourceConverter();
    }

    @Provides
    @FeedScope
    ResourceEntityConverter provideResourceEntityConverter() {
        return new ResourceEntityConverter();
    }
}
