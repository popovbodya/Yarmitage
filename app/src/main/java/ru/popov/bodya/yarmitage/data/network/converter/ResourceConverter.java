package ru.popov.bodya.yarmitage.data.network.converter;

import android.support.annotation.NonNull;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import ru.popov.bodya.yarmitage.data.network.beans.ResourceBean;
import ru.popov.bodya.yarmitage.data.network.beans.ResourceListBean;
import ru.popov.bodya.yarmitage.domain.global.models.Resource;

/**
 * @author popovbodya
 */
public class ResourceConverter {

    @NonNull
    public List<Resource> convertList(ResourceListBean resourceListBean) {

        if (resourceListBean == null || resourceListBean.getItems().isEmpty()) {
            return Collections.emptyList();
        }

        final List<Resource> resourceList = new ArrayList<>();
        for (ResourceBean resourceBean : resourceListBean.getItems()) {
            resourceList.add(convert(resourceBean));
        }
        return resourceList;
    }

    @NonNull
    private Resource convert(ResourceBean resourceBean) {
        return new Resource(
                resourceBean.getResourceId(),
                resourceBean.getName(),
                resourceBean.getPreview(),
                resourceBean.getFile(),
                resourceBean.getPath(),
                resourceBean.getMediaType(),
                resourceBean.getSize()
        );
    }
}
