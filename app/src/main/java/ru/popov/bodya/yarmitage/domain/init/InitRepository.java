package ru.popov.bodya.yarmitage.domain.init;

import io.reactivex.Single;

/**
 * @author popovbodya
 */

public interface InitRepository {

    Single<String> getToken();

}
