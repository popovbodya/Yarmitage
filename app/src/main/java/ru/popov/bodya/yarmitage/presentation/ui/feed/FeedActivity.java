package ru.popov.bodya.yarmitage.presentation.ui.feed;

import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.util.DiffUtil;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.arellomobile.mvp.presenter.InjectPresenter;
import com.arellomobile.mvp.presenter.ProvidePresenter;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.model.LazyHeaders;
import com.bumptech.glide.request.RequestOptions;
import com.yandex.disk.rest.Credentials;

import java.util.List;

import javax.inject.Inject;

import ru.popov.bodya.yarmitage.R;
import ru.popov.bodya.yarmitage.di.feed.FeedInjector;
import ru.popov.bodya.yarmitage.domain.global.models.Resource;
import ru.popov.bodya.yarmitage.presentation.mvp.feed.FeedPresenter;
import ru.popov.bodya.yarmitage.presentation.mvp.feed.FeedView;
import ru.popov.bodya.yarmitage.presentation.mvp.feed.TokenHeaderHolder;
import ru.popov.bodya.yarmitage.presentation.ui.about.AboutActivity;
import ru.popov.bodya.yarmitage.presentation.ui.feed.adapter.FeedAdapter;
import ru.popov.bodya.yarmitage.presentation.ui.global.BaseCoreActivity;
import ru.popov.bodya.yarmitage.presentation.ui.global.Screens;
import ru.popov.bodya.yarmitage.presentation.ui.viewer.activity.ImageViewerActivity;
import ru.terrakok.cicerone.Navigator;
import ru.terrakok.cicerone.NavigatorHolder;
import ru.terrakok.cicerone.android.SupportAppNavigator;

import static android.support.v7.widget.StaggeredGridLayoutManager.VERTICAL;

/**
 * @author popovbodya
 */

public class FeedActivity extends BaseCoreActivity implements FeedView, FeedAdapter.OnImageClickListener, SwipeRefreshLayout.OnRefreshListener, FeedAdapter.OnLoadMoreListener, NavigationView.OnNavigationItemSelectedListener {

    public static final String ARG_TOKEN_KEY = "arg_token_key";
    private static final String USER_NAME = "bodya";

    @Inject
    @InjectPresenter
    FeedPresenter mFeedPresenter;

    @Inject
    NavigatorHolder mNavigatorHolder;
    @Inject
    TokenHeaderHolder mTokenHeaderHolder;

    private DrawerLayout mDrawerLayout;
    private SwipeRefreshLayout mSwipeRefreshLayout;
    private RecyclerView mRecyclerView;
    private FeedAdapter mFeedAdapter;
    private ProgressBar mProgressBar;
    private ViewGroup mZeroLayout;

    @ProvidePresenter
    public FeedPresenter provideFeedPresenter() {
        return mFeedPresenter;
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {

        injectDependencies();
        super.onCreate(savedInstanceState);
        setContentView(R.layout.feed_activity_layout);
        initViews();
    }

    @Override
    protected void onResume() {
        super.onResume();
        mNavigatorHolder.setNavigator(mNavigator);
    }

    @Override
    protected void onPause() {
        mNavigatorHolder.removeNavigator();
        super.onPause();
    }

    @Override
    protected void onDestroy() {
        if (isFinishing()) {
            FeedInjector.clearFeedSubComponent();
        }
        super.onDestroy();
    }

    @Override
    public void onBackPressed() {
        if (mDrawerLayout.isDrawerOpen(GravityCompat.START)) {
            mDrawerLayout.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public void takeData(boolean show, List<Resource> resourcesList) {
        mRecyclerView.setVisibility(show ? View.VISIBLE : View.GONE);
        mFeedPresenter.onDiffEvaluateRequest(mFeedAdapter.getCurrentList(), resourcesList);
    }

    @Override
    public void showDiffData(List<Resource> newList, DiffUtil.DiffResult diffResult) {
        diffResult.dispatchUpdatesTo(mFeedAdapter);
        mFeedAdapter.setList(newList, diffResult);
    }

    @Override
    public void showRefreshProgress(boolean show) {
        mSwipeRefreshLayout.post(() -> mSwipeRefreshLayout.setRefreshing(show));
    }

    @Override
    public void showFullProgress(boolean show) {
        mProgressBar.setVisibility(show ? View.VISIBLE : View.GONE);
        mSwipeRefreshLayout.setVisibility(!show ? View.VISIBLE : View.GONE);
        mSwipeRefreshLayout.post(() -> mSwipeRefreshLayout.setRefreshing(false));
    }

    @Override
    public void showPageProgress(boolean show) {
        mSwipeRefreshLayout.post(() -> mSwipeRefreshLayout.setRefreshing(show));
    }

    @Override
    public void showZeroView(boolean show) {
        mZeroLayout.setVisibility(show ? View.VISIBLE : View.GONE);
    }

    @Override
    public void showErrorMessage() {
        Toast
                .makeText(getApplicationContext(), R.string.feed_toast_message, Toast.LENGTH_LONG)
                .show();
    }

    @Override
    public void onImageClick(int position) {
        mFeedPresenter.onImageClick(position);
    }

    @Override
    public void onRefresh() {
        mFeedPresenter.onRefresh();
    }

    @Override
    public void onLoadMore() {
        mFeedPresenter.onLoadMore();
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        final int id = item.getItemId();
        if (id == R.id.nav_info) {
            mFeedPresenter.onAboutOptionClick();
        }
        mDrawerLayout.closeDrawer(GravityCompat.START);
        return true;
    }

    private void injectDependencies() {
        Credentials credentials;
        final String token = getIntent().getStringExtra(ARG_TOKEN_KEY);
        if (token != null) {
            credentials = new Credentials(USER_NAME, token);
            FeedInjector
                    .getFeedSubComponent(this, credentials, token)
                    .inject(this);
        } else {
            throw new IllegalArgumentException("Cannot find extras: " + ARG_TOKEN_KEY);
        }
    }

    private void initViews() {
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        mDrawerLayout = findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, mDrawerLayout, toolbar, R.string.feed_drawer_open_content_desc, R.string.feed_drawer_close_content_desc);
        mDrawerLayout.addDrawerListener(toggle);
        toggle.syncState();
        NavigationView navigationView = findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        mZeroLayout = findViewById(R.id.zero_layout);
        Button zeroRefreshButton = findViewById(R.id.refresh_button);
        zeroRefreshButton.setOnClickListener(v -> mFeedPresenter.onRefresh());
        mProgressBar = findViewById(R.id.progress_bar);
        mSwipeRefreshLayout = findViewById(R.id.feed_swipe_refresh);
        mSwipeRefreshLayout.setOnRefreshListener(this);
        mRecyclerView = findViewById(R.id.feed_recycler_view);
        mRecyclerView.setHasFixedSize(true);
        StaggeredGridLayoutManager mGridLayoutManager = new StaggeredGridLayoutManager(2, VERTICAL);
        mRecyclerView.setLayoutManager(mGridLayoutManager);
        mFeedAdapter = new FeedAdapter(Glide.with(this), createRequestOptions(), createHeaders(), this, this);
        mRecyclerView.setAdapter(mFeedAdapter);
    }

    private LazyHeaders createHeaders() {
        return new LazyHeaders.Builder()
                .addHeader(mTokenHeaderHolder.getHeaderKey(), mTokenHeaderHolder.getHeaderValue())
                .build();
    }

    private RequestOptions createRequestOptions() {
        return new RequestOptions()
                .optionalCenterCrop()
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .placeholder(new ColorDrawable(ContextCompat.getColor(this, R.color.colorPrimary)));
    }

    private Navigator mNavigator = new SupportAppNavigator(this, 0) {
        @Override
        protected Intent createActivityIntent(Context context, String screenKey, Object data) {
            switch (screenKey) {
                case Screens.IMAGE_VIEWER_SCREEN:
                    return ImageViewerActivity.newIntent(FeedActivity.this, (Integer) data, mFeedAdapter.getUriList());
                case Screens.ABOUT_SCREEN:
                    return new Intent(FeedActivity.this, AboutActivity.class);
                default:
                    return null;
            }
        }

        @Override
        protected Fragment createFragment(String screenKey, Object data) {
            return null;
        }
    };
}
