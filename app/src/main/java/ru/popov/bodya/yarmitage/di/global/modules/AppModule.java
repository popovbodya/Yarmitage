package ru.popov.bodya.yarmitage.di.global.modules;

import android.content.Context;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import ru.popov.bodya.yarmitage.app.YarmitageApp;
import ru.popov.bodya.yarmitage.di.global.ApplicationContext;

/**
 * @author popovbodya
 */
@Module
public class AppModule {

    private final YarmitageApp mYarmitageApp;

    public AppModule(YarmitageApp yarmitageApp) {
        mYarmitageApp = yarmitageApp;
    }

    @Provides
    @ApplicationContext
    @Singleton
    Context provideApplicationContext() {
        return mYarmitageApp;
    }

}
