package ru.popov.bodya.yarmitage.di.auth.modules;

import dagger.Module;
import dagger.Provides;
import ru.popov.bodya.yarmitage.app.rx.RxSchedulersTransformer;
import ru.popov.bodya.yarmitage.di.auth.AuthScope;
import ru.popov.bodya.yarmitage.domain.auth.AuthInteractor;
import ru.popov.bodya.yarmitage.presentation.mvp.auth.AuthPresenter;
import ru.terrakok.cicerone.Router;

/**
 * @author popovbodya
 */
@Module
public class AuthPresenterModule {

    @AuthScope
    @Provides
    AuthPresenter provideAuthPresenter(Router router, AuthInteractor initInteractor, RxSchedulersTransformer rxSchedulersTransformer) {
        return new AuthPresenter(router, initInteractor, rxSchedulersTransformer);
    }

}
