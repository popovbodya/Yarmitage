package ru.popov.bodya.yarmitage.di.init.modules;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.preference.PreferenceManager;

import dagger.Module;
import dagger.Provides;
import ru.popov.bodya.yarmitage.data.database.preferences.SharedPreferencesWrapper;
import ru.popov.bodya.yarmitage.data.repositories.InitRepositoryImpl;
import ru.popov.bodya.yarmitage.di.global.ApplicationContext;
import ru.popov.bodya.yarmitage.di.init.InitScope;
import ru.popov.bodya.yarmitage.domain.init.InitRepository;

/**
 * @author popovbodya
 */
@Module
public class InitRepositoryModule {

    @InitScope
    @Provides
    InitRepository provideInitRepository(SharedPreferencesWrapper sharedPreferencesWrapper) {
        return new InitRepositoryImpl(sharedPreferencesWrapper);
    }

    @InitScope
    @Provides
    SharedPreferencesWrapper provideSharedPreferencesWrapper(SharedPreferences sharedPreferences, ApplicationInfo applicationInfo) {
        return new SharedPreferencesWrapper(sharedPreferences, applicationInfo);
    }

    @InitScope
    @Provides
    SharedPreferences provideSharedPreferences(@ApplicationContext Context context) {
        return PreferenceManager.getDefaultSharedPreferences(context);
    }

    @InitScope
    @Provides
    ApplicationInfo provideApplicationInfo(@ApplicationContext Context context) {
        try {
            return context.getPackageManager().getApplicationInfo(context.getPackageName(), PackageManager.GET_META_DATA);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        return null;
    }
}
