package ru.popov.bodya.yarmitage.app;

import android.app.Application;

import ru.popov.bodya.yarmitage.di.global.AppComponent;
import ru.popov.bodya.yarmitage.di.global.DaggerAppComponent;
import ru.popov.bodya.yarmitage.di.global.IHasComponent;
import ru.popov.bodya.yarmitage.di.global.modules.AppModule;

/**
 * @author popovbodya
 */

public class YarmitageApp extends Application implements IHasComponent<AppComponent> {

    private AppComponent mAppComponent;

    @Override
    public void onCreate() {
        super.onCreate();
        mAppComponent = DaggerAppComponent
                .builder()
                .appModule(new AppModule(this))
                .build();
    }

    @Override
    public AppComponent getComponent() {
        return mAppComponent;
    }

}

