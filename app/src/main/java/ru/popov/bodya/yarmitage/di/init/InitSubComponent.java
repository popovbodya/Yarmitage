package ru.popov.bodya.yarmitage.di.init;

import dagger.BindsInstance;
import dagger.Subcomponent;
import ru.popov.bodya.yarmitage.di.init.modules.InitInteractorModule;
import ru.popov.bodya.yarmitage.di.init.modules.InitPresenterModule;
import ru.popov.bodya.yarmitage.di.init.modules.InitRepositoryModule;
import ru.popov.bodya.yarmitage.presentation.ui.init.activity.InitActivity;

/**
 * @author popovbodya
 */
@InitScope
@Subcomponent(modules = {InitPresenterModule.class, InitRepositoryModule.class, InitInteractorModule.class})
public interface InitSubComponent {

    @Subcomponent.Builder
    interface Builder {
        InitSubComponent build();
    }

    void inject(InitActivity initActivity);

}
