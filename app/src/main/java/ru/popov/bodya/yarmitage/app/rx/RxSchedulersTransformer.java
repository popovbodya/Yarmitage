package ru.popov.bodya.yarmitage.app.rx;

import io.reactivex.CompletableTransformer;
import io.reactivex.FlowableTransformer;
import io.reactivex.MaybeTransformer;
import io.reactivex.ObservableTransformer;
import io.reactivex.SingleTransformer;

/**
 * @author popovbodya
 */

public interface RxSchedulersTransformer {

    /**
     * Трансформер для Observable, переключает IO на main
     *
     * @param <T> тип внутри Observable
     * @return {@link ObservableTransformer}
     */
    <T> ObservableTransformer<T, T> getIOToMainTransformer();

    /**
     * Трансформер для Single, переключает c IO на main
     *
     * @param <T> тип внутри Single
     * @return {@link SingleTransformer}
     */
    <T> SingleTransformer<T, T> getIOToMainTransformerSingle();

    /**
     * Трансформер для Maybe, переключает c IO на main
     *
     * @param <T> тип внутри Maybe
     * @return {@link MaybeTransformer}
     */
    <T> MaybeTransformer<T, T> getIOToMainTransformerMaybe();

    /**
     * Трансформер для Completable, переключает c IO на main
     *
     * @return {@link CompletableTransformer}
     */
    CompletableTransformer getIOToMainTransformerCompletable();

    /**
     * Трансформер для Flowable, переключает c IO на main
     *
     * @param <T> тип внутри Flowable
     * @return {@link FlowableTransformer}
     */
    <T> FlowableTransformer<T, T> getIOToMainTransformerFlowable();


    /**
     * Трансформер для Single, переключает с Computation на main
     *
     * @param <T> <T> тип внутри Single
     * @return {@link SingleTransformer}
     */
    <T> SingleTransformer<T, T> getComputationToMainTransformerSingle();

}
