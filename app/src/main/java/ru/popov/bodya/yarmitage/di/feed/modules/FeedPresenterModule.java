package ru.popov.bodya.yarmitage.di.feed.modules;

import dagger.Module;
import dagger.Provides;
import ru.popov.bodya.yarmitage.app.rx.RxSchedulersTransformer;
import ru.popov.bodya.yarmitage.di.feed.FeedScope;
import ru.popov.bodya.yarmitage.domain.feed.FeedInteractor;
import ru.popov.bodya.yarmitage.presentation.mvp.feed.FeedPresenter;
import ru.popov.bodya.yarmitage.presentation.mvp.feed.TokenHeaderHolder;
import ru.terrakok.cicerone.Router;

/**
 * @author popovbodya
 */
@Module
public class FeedPresenterModule {

    @FeedScope
    @Provides
    FeedPresenter provideFeedPresenter(Router router, FeedInteractor feedInteractor, RxSchedulersTransformer rxSchedulersTransformer) {
        return new FeedPresenter(router, feedInteractor, rxSchedulersTransformer);
    }

    @FeedScope
    @Provides
    TokenHeaderHolder provideTokenHolder(String token) {
        return new TokenHeaderHolder(token);
    }

}
