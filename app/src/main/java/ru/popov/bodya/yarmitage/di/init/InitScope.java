package ru.popov.bodya.yarmitage.di.init;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

import javax.inject.Scope;

/**
 * @author popovbodya
 */

@Scope
@Retention(RetentionPolicy.RUNTIME)
public @interface InitScope {
}
