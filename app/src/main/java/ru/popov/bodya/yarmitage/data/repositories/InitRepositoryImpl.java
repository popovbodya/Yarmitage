package ru.popov.bodya.yarmitage.data.repositories;

import android.content.SharedPreferences;
import android.content.pm.ApplicationInfo;

import io.reactivex.Single;
import ru.popov.bodya.yarmitage.data.database.preferences.SharedPreferencesWrapper;
import ru.popov.bodya.yarmitage.domain.init.InitRepository;

/**
 * @author popovbodya
 */

public class InitRepositoryImpl implements InitRepository {

    private final SharedPreferencesWrapper mSharedPreferencesWrapper;

    public InitRepositoryImpl(SharedPreferencesWrapper sharedPreferencesWrapper) {
        mSharedPreferencesWrapper = sharedPreferencesWrapper;
    }

    @Override
    public Single<String> getToken() {
        return Single.fromCallable(mSharedPreferencesWrapper::getToken);
    }
}
