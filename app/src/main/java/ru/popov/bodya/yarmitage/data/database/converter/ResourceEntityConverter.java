package ru.popov.bodya.yarmitage.data.database.converter;


import android.support.annotation.NonNull;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import ru.popov.bodya.yarmitage.data.database.entities.ResourceEntity;
import ru.popov.bodya.yarmitage.domain.global.models.Resource;

/**
 * @author popovbodya
 */
public class ResourceEntityConverter {

    @NonNull
    public List<ResourceEntity> convertList(List<Resource> resourceList) {

        if (resourceList == null || resourceList.isEmpty()) {
            return Collections.emptyList();
        }

        final List<ResourceEntity> resourceEntityList = new ArrayList<>();
        for (Resource resource : resourceList) {
            resourceEntityList.add(convert(resource));
        }
        return resourceEntityList;
    }

    @NonNull
    public List<Resource> reverseList(List<ResourceEntity> resourceEntityList) {

        if (resourceEntityList == null || resourceEntityList.isEmpty()) {
            return Collections.emptyList();
        }

        final List<Resource> resourceList = new ArrayList<>();
        for (ResourceEntity resourceEntity : resourceEntityList) {
            resourceList.add(reverse(resourceEntity));
        }
        return resourceList;
    }

    @NonNull
    private ResourceEntity convert(Resource resource) {
        return new ResourceEntity(
                resource.getResourceId(),
                resource.getName(),
                resource.getPreview(),
                resource.getFile(),
                resource.getPath(),
                resource.getMediaType(),
                resource.getSize()
        );
    }

    @NonNull
    private Resource reverse(ResourceEntity resourceEntity) {
        return new Resource(
                resourceEntity.getResourceId(),
                resourceEntity.getName(),
                resourceEntity.getPreview(),
                resourceEntity.getFile(),
                resourceEntity.getPath(),
                resourceEntity.getMediaType(),
                resourceEntity.getSize()
        );
    }

}
