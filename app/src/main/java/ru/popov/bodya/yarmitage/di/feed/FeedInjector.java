package ru.popov.bodya.yarmitage.di.feed;

import android.content.Context;

import com.yandex.disk.rest.Credentials;

import ru.popov.bodya.yarmitage.di.global.AppComponent;
import ru.popov.bodya.yarmitage.di.global.ComponentUtils;

/**
 * @author popovbodya
 */

public class FeedInjector {

    private static FeedSubComponent sFeedSubComponent = null;

    private FeedInjector() {
        throw new UnsupportedOperationException("No instances");
    }

    public static FeedSubComponent getFeedSubComponent(Context context, Credentials credentials, String token) {
        if (sFeedSubComponent == null) {
            sFeedSubComponent = ComponentUtils.getComponent(context, AppComponent.class)
                    .feedSubComponentBuilder()
                    .credentials(credentials)
                    .token(token)
                    .build();
        }
        return sFeedSubComponent;
    }

    public static void clearFeedSubComponent() {
        sFeedSubComponent = null;
    }

}
