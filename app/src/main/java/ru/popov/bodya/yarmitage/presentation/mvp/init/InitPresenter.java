package ru.popov.bodya.yarmitage.presentation.mvp.init;

import com.arellomobile.mvp.InjectViewState;

import io.reactivex.disposables.Disposable;
import ru.popov.bodya.yarmitage.app.rx.RxSchedulersTransformer;
import ru.popov.bodya.yarmitage.domain.init.InitInteractor;
import ru.popov.bodya.yarmitage.presentation.mvp.global.view.AppPresenter;
import ru.terrakok.cicerone.Router;

import static ru.popov.bodya.yarmitage.presentation.ui.global.Screens.AUTH_SCREEN;
import static ru.popov.bodya.yarmitage.presentation.ui.global.Screens.FEED_SCREEN;

/**
 * @author popovbodya
 */
@InjectViewState
public class InitPresenter extends AppPresenter<InitView> {

    private final Router mRouter;
    private final InitInteractor mInitInteractor;
    private final RxSchedulersTransformer mRxSchedulersTransformer;

    public InitPresenter(Router router, InitInteractor initInteractor, RxSchedulersTransformer rxSchedulersTransformer) {
        mRouter = router;
        mInitInteractor = initInteractor;
        mRxSchedulersTransformer = rxSchedulersTransformer;
    }

    @Override
    protected void onFirstViewAttach() {
        super.onFirstViewAttach();
        getToken();
    }

    private void getToken() {
        Disposable disposable = mInitInteractor
                .getToken()
                .compose(mRxSchedulersTransformer.getIOToMainTransformerSingle())
                .subscribe(this::onTokenReceived);
        connect(disposable);
    }

    private void onTokenReceived(String token) {
        if ("".equals(token)) {
            mRouter.newRootScreen(AUTH_SCREEN);
        } else {
            mRouter.newRootScreen(FEED_SCREEN, token);
        }
    }
}
