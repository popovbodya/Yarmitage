package ru.popov.bodya.yarmitage.di.auth.modules;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.preference.PreferenceManager;

import com.yandex.authsdk.YandexAuthOptions;
import com.yandex.authsdk.YandexAuthSdk;

import dagger.Module;
import dagger.Provides;
import ru.popov.bodya.yarmitage.data.database.preferences.SharedPreferencesWrapper;
import ru.popov.bodya.yarmitage.data.network.api.sdk.YandexSdkWrapper;
import ru.popov.bodya.yarmitage.data.repositories.AuthRepositoryImpl;
import ru.popov.bodya.yarmitage.di.auth.AuthScope;
import ru.popov.bodya.yarmitage.di.global.ApplicationContext;
import ru.popov.bodya.yarmitage.domain.auth.AuthRepository;

/**
 * @author popovbodya
 */
@Module
public class AuthRepositoryModule {

    @AuthScope
    @Provides
    AuthRepository provideAuthRepository(SharedPreferencesWrapper sharedPreferencesWrapper, YandexSdkWrapper yandexSdkWrapper) {
        return new AuthRepositoryImpl(sharedPreferencesWrapper, yandexSdkWrapper);
    }

    @AuthScope
    @Provides
    YandexSdkWrapper provideYandexSdkWrapper(@ApplicationContext Context appContext, YandexAuthSdk yandexAuthSdk) {
        return new YandexSdkWrapper(appContext, yandexAuthSdk);
    }

    @AuthScope
    @Provides
    SharedPreferencesWrapper provideSharedPreferencesWrapper(SharedPreferences sharedPreferences, ApplicationInfo applicationInfo) {
        return new SharedPreferencesWrapper(sharedPreferences, applicationInfo);
    }

    @AuthScope
    @Provides
    SharedPreferences provideSharedPreferences(@ApplicationContext Context context) {
        return PreferenceManager.getDefaultSharedPreferences(context);
    }

    @AuthScope
    @Provides
    YandexAuthSdk provideYandexAuthSdk(@ApplicationContext Context context, YandexAuthOptions yandexAuthOptions) {
        return new YandexAuthSdk(context, yandexAuthOptions);
    }

    @AuthScope
    @Provides
    YandexAuthOptions provideYandexAuthOptions(@ApplicationContext Context context) {
        return new YandexAuthOptions(context, true);
    }

    @AuthScope
    @Provides
    ApplicationInfo provideApplicationInfo(@ApplicationContext Context context) {
        try {
            return context.getPackageManager().getApplicationInfo(context.getPackageName(), PackageManager.GET_META_DATA);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        return null;
    }

}
