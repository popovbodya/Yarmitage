package ru.popov.bodya.yarmitage.di.init;

import android.content.Context;

import ru.popov.bodya.yarmitage.di.global.AppComponent;
import ru.popov.bodya.yarmitage.di.global.ComponentUtils;

/**
 * @author popovbodya
 */

public class InitInjector {

    private static InitSubComponent sInitSubComponent = null;

    private InitInjector() {
        throw new UnsupportedOperationException("No instances");
    }

    public static InitSubComponent getFeedSubComponent(Context context) {
        if (sInitSubComponent == null) {
            sInitSubComponent = ComponentUtils.getComponent(context, AppComponent.class)
                    .initSubComponentBuilder()
                    .build();
        }
        return sInitSubComponent;
    }

    public static void clearInitSubComponent() {
        sInitSubComponent = null;
    }

}
