package ru.popov.bodya.yarmitage.domain.feed;

import android.support.v7.util.DiffUtil;
import android.util.Log;

import java.util.List;

import ru.popov.bodya.yarmitage.domain.global.models.Resource;

/**
 * @author popovbodya
 */

public class ResourcesDiffUtilCallback extends DiffUtil.Callback {

    private final List<Resource> mOldList;
    private final List<Resource> mNewList;

    public ResourcesDiffUtilCallback(List<Resource> oldList, List<Resource> newList) {
        mOldList = oldList;
        mNewList = newList;
    }

    @Override
    public int getOldListSize() {
        return mOldList.size();
    }

    @Override
    public int getNewListSize() {
        return mNewList.size();
    }

    @Override
    public boolean areItemsTheSame(int oldItemPosition, int newItemPosition) {
        final Resource oldItem = mOldList.get(oldItemPosition);
        final Resource newItem = mNewList.get(newItemPosition);
        return oldItem.getResourceId().equals(newItem.getResourceId());
    }

    @Override
    public boolean areContentsTheSame(int oldItemPosition, int newItemPosition) {
        final Resource oldItem = mOldList.get(oldItemPosition);
        final Resource newItem = mNewList.get(newItemPosition);
        return oldItem.getName().equals(newItem.getName());
    }
}
