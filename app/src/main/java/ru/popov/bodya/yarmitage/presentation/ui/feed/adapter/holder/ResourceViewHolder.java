package ru.popov.bodya.yarmitage.presentation.ui.feed.adapter.holder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;

import com.bumptech.glide.RequestManager;
import com.bumptech.glide.load.model.GlideUrl;
import com.bumptech.glide.load.model.Headers;
import com.bumptech.glide.request.RequestOptions;

import ru.popov.bodya.yarmitage.R;
import ru.popov.bodya.yarmitage.presentation.ui.feed.adapter.FeedAdapter;

/**
 * @author popovbodya
 */

public class ResourceViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

    private final FeedAdapter.OnImageClickListener mOnImageClickListener;
    private final ImageView mImageView;
    private final Headers mHeaders;
    private final RequestManager mRequestManager;
    private final RequestOptions mRequestOptions;

    private int mCurrentPosition;

    public ResourceViewHolder(View itemView, Headers headers, RequestManager requestManager, RequestOptions requestOptions, FeedAdapter.OnImageClickListener onImageClickListener) {
        super(itemView);

        mOnImageClickListener = onImageClickListener;
        mHeaders = headers;
        mRequestManager = requestManager;
        mRequestOptions = requestOptions;
        mImageView = itemView.findViewById(R.id.cell_image_view);
        itemView.setOnClickListener(this);
    }

    public void bind(String url, int position) {

        mCurrentPosition = position;

        mRequestManager
                .load(new GlideUrl(url, mHeaders))
                .apply(mRequestOptions)
                .into(mImageView);
    }

    @Override
    public void onClick(View v) {
        if (mOnImageClickListener != null) {
            mOnImageClickListener.onImageClick(mCurrentPosition);
        }
    }
}