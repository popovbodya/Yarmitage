package ru.popov.bodya.yarmitage.data.database.entities


data class ResourceEntity(
        val resourceId: String,
        val name: String,
        val preview: String?,
        val file: String,
        val path: String,
        val mediaType: String,
        val size: Int
)