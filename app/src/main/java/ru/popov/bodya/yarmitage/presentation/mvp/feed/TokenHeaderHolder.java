package ru.popov.bodya.yarmitage.presentation.mvp.feed;

/**
 * @author popovbodya
 */

public class TokenHeaderHolder {

    private static final String AUTHORIZATION = "Authorization";
    private static final String TOKEN_TYPE = "OAuth";
    private final String mToken;

    public TokenHeaderHolder(String token) {
        mToken = token;
    }

    public String getHeaderKey() {
        return AUTHORIZATION;
    }

    public String getHeaderValue() {
        return String.format("%s %s", TOKEN_TYPE, mToken);
    }

}
