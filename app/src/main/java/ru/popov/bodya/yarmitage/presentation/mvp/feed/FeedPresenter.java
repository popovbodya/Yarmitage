package ru.popov.bodya.yarmitage.presentation.mvp.feed;

import com.arellomobile.mvp.InjectViewState;

import java.util.List;

import io.reactivex.Flowable;
import io.reactivex.Single;
import io.reactivex.disposables.Disposable;
import ru.popov.bodya.yarmitage.app.rx.RxSchedulersTransformer;
import ru.popov.bodya.yarmitage.domain.feed.FeedInteractor;
import ru.popov.bodya.yarmitage.domain.feed.ResourcesDiffUtilCallback;
import ru.popov.bodya.yarmitage.domain.global.models.Resource;
import ru.popov.bodya.yarmitage.presentation.mvp.global.pagination.PaginationViewController;
import ru.popov.bodya.yarmitage.presentation.mvp.global.pagination.Paginator;
import ru.popov.bodya.yarmitage.presentation.mvp.global.pagination.RequestFactory;
import ru.popov.bodya.yarmitage.presentation.mvp.global.view.AppPresenter;
import ru.popov.bodya.yarmitage.presentation.ui.global.Screens;
import ru.terrakok.cicerone.Router;


/**
 * @author popovbodya
 */
@InjectViewState
public class FeedPresenter extends AppPresenter<FeedView> {

    private final Router mRouter;
    private final FeedInteractor mFeedInteractor;
    private final RxSchedulersTransformer mRxSchedulersTransformer;

    public FeedPresenter(Router router, FeedInteractor feedInteractor, RxSchedulersTransformer rxSchedulersTransformer) {
        mRouter = router;
        mFeedInteractor = feedInteractor;
        mRxSchedulersTransformer = rxSchedulersTransformer;
    }

    @Override
    protected void onFirstViewAttach() {
        super.onFirstViewAttach();
        mResourcePaginator.refresh();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mResourcePaginator.release();
    }

    public void onRefresh() {
        mResourcePaginator.refresh();
    }

    public void onLoadMore() {
        mResourcePaginator.loadNewPage();
    }

    public void onDiffEvaluateRequest(List<Resource> oldList, List<Resource> newList) {
        final Disposable disposable = mFeedInteractor
                .evaluateDiff(new ResourcesDiffUtilCallback(oldList, newList))
                .compose(mRxSchedulersTransformer.getComputationToMainTransformerSingle())
                .subscribe((diffResult, throwable) -> {
                    if (throwable == null && diffResult != null) {
                        getViewState().showDiffData(newList, diffResult);
                    }
                });
        connect(disposable);
    }

    public void onAboutOptionClick() {
        mRouter.navigateTo(Screens.ABOUT_SCREEN);
    }

    public void onImageClick(int position) {
        mRouter.navigateTo(Screens.IMAGE_VIEWER_SCREEN, position);
    }

    private Paginator<Resource> mResourcePaginator = new Paginator<>(new RequestFactory<Resource>() {
        @Override
        public Single<List<Resource>> makeRequest(int page) {
            return mFeedInteractor
                    .getImages(page)
                    .compose(mRxSchedulersTransformer.getIOToMainTransformerSingle());
        }

        @Override
        public Flowable<List<Resource>> makeInitialRequest() {
            return mFeedInteractor
                    .initialFetchDataFromAllSources()
                    .compose(mRxSchedulersTransformer.getIOToMainTransformerFlowable());
        }
    }, new PaginationViewController<Resource>() {
        @Override
        public void showFullProgress(boolean show) {
            getViewState().showFullProgress(show);
        }

        @Override
        public void showEmptyView(boolean show) {
            getViewState().showZeroView(show);
        }

        @Override
        public void showData(boolean show, List<Resource> data) {
            getViewState().takeData(show, data);
        }

        @Override
        public void showRefreshProgress(boolean show) {
            getViewState().showRefreshProgress(show);
        }

        @Override
        public void showPageProgress(boolean show) {
            getViewState().showPageProgress(show);
        }

        @Override
        public void showErrorMessage(Throwable error) {
            getViewState().showErrorMessage();
        }
    });

}
