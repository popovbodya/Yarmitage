package ru.popov.bodya.yarmitage.data.network.beans

import com.google.gson.annotations.SerializedName

/**
 * @author popovbodya
 */
data class ResourceListBean(
        @SerializedName("items") val items: List<ResourceBean>
)