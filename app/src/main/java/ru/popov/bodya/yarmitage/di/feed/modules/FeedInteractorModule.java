package ru.popov.bodya.yarmitage.di.feed.modules;

import dagger.Module;
import dagger.Provides;
import ru.popov.bodya.yarmitage.app.rx.RxSchedulers;
import ru.popov.bodya.yarmitage.di.feed.FeedScope;
import ru.popov.bodya.yarmitage.domain.feed.FeedInteractor;
import ru.popov.bodya.yarmitage.domain.feed.FeedRepository;

/**
 * @author popovbodya
 */
@Module
public class FeedInteractorModule {

    @Provides
    @FeedScope
    FeedInteractor provideFeedInteractor(FeedRepository feedRepository, RxSchedulers rxSchedulers) {
        return new FeedInteractor(feedRepository, rxSchedulers);
    }
}
