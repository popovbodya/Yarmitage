package ru.popov.bodya.yarmitage.presentation.ui.auth.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.arellomobile.mvp.presenter.InjectPresenter;
import com.arellomobile.mvp.presenter.ProvidePresenter;
import com.bumptech.glide.Glide;

import javax.inject.Inject;

import ru.popov.bodya.yarmitage.R;
import ru.popov.bodya.yarmitage.di.auth.AuthInjector;
import ru.popov.bodya.yarmitage.presentation.mvp.auth.AuthPresenter;
import ru.popov.bodya.yarmitage.presentation.mvp.auth.AuthView;
import ru.popov.bodya.yarmitage.presentation.ui.feed.FeedActivity;
import ru.popov.bodya.yarmitage.presentation.ui.global.BaseCoreActivity;
import ru.popov.bodya.yarmitage.presentation.ui.global.Screens;
import ru.terrakok.cicerone.Navigator;
import ru.terrakok.cicerone.NavigatorHolder;
import ru.terrakok.cicerone.android.SupportAppNavigator;
import ru.terrakok.cicerone.commands.Forward;

import static ru.popov.bodya.yarmitage.presentation.mvp.auth.AuthConstants.REQUEST_LOGIN_SDK;

public class AuthActivity extends BaseCoreActivity implements AuthView {

    @Inject
    @InjectPresenter
    AuthPresenter mAuthPresenter;
    @Inject
    NavigatorHolder mNavigatorHolder;

    private ProgressBar mProgressBar;
    private TextView mPromoTextView;

    @ProvidePresenter
    public AuthPresenter provideAuthPresenter() {
        return mAuthPresenter;
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {

        AuthInjector
                .getAuthSubComponent(this)
                .inject(this);

        super.onCreate(savedInstanceState);
        setContentView(R.layout.auth_activity_layout);
        initViews();
    }

    @Override
    protected void onResume() {
        super.onResume();
        mNavigatorHolder.setNavigator(mNavigator);
    }

    @Override
    protected void onPause() {
        mNavigatorHolder.removeNavigator();
        super.onPause();
    }

    @Override
    protected void onDestroy() {
        if (isFinishing()) {
            AuthInjector.clearAuthSubComponent();
        }
        super.onDestroy();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_LOGIN_SDK) {
            mAuthPresenter.onActivityResult(resultCode, data);
        } else {
            super.onActivityResult(requestCode, resultCode, data);
        }
    }

    @Override
    public void showLoading(boolean show) {
        mProgressBar.setVisibility(show ? View.VISIBLE : View.GONE);
        mPromoTextView.setVisibility(show ? View.GONE : View.VISIBLE);
    }

    private Navigator mNavigator = new SupportAppNavigator(this, 0) {

        @Override
        protected void forward(Forward command) {
            final String screenKey = command.getScreenKey();
            switch (screenKey) {
                case Screens.YANDEX_LOGIN_SCREEN:
                    forwardToYandexLoginScreen(command);
                    break;
                default:
                    super.forward(command);
            }
        }

        @Override
        protected Intent createActivityIntent(Context context, String screenKey, Object data) {
            switch (screenKey) {
                case Screens.FEED_SCREEN:
                    final Intent intent = new Intent(AuthActivity.this, FeedActivity.class);
                    intent.putExtra(FeedActivity.ARG_TOKEN_KEY, (String) data);
                    return intent;
                default:
                    return null;
            }
        }

        @Override
        protected Fragment createFragment(String screenKey, Object data) {
            return null;
        }

        private void forwardToYandexLoginScreen(Forward command) {
            final Object data = command.getTransitionData();
            if (data instanceof Intent) {
                startActivityForResult((Intent) data, REQUEST_LOGIN_SDK);
            } else {
                throw new IllegalArgumentException("data should be Intent");
            }
        }
    };

    private void initViews() {

        mProgressBar = findViewById(R.id.progress_bar);
        mPromoTextView = findViewById(R.id.promo_text_view);

        Button enablePermissionsButton = findViewById(R.id.enable_permissions_button);
        enablePermissionsButton.setOnClickListener(v -> mAuthPresenter.onEnablePermissionsButtonClick());

        ImageView imageView = findViewById(R.id.welcome_image_view);
        Glide.with(this).load(R.drawable.gallery_image).into(imageView);
    }
}
