package ru.popov.bodya.yarmitage.di.auth;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

import javax.inject.Scope;

/**
 * @author popovbodya
 */
@Scope
@Retention(RetentionPolicy.RUNTIME)
public @interface AuthScope {
}
