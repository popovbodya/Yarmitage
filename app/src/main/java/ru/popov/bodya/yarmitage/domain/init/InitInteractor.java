package ru.popov.bodya.yarmitage.domain.init;

import android.content.Context;

import io.reactivex.Single;

/**
 * @author popovbodya
 */

public class InitInteractor {

    private final InitRepository mInitRepository;

    public InitInteractor(InitRepository initRepository) {
        mInitRepository = initRepository;
    }

    public Single<String> getToken() {
        return mInitRepository.getToken();
    }
}
