package ru.popov.bodya.yarmitage.presentation.mvp.viewer;

import com.arellomobile.mvp.InjectViewState;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import ru.popov.bodya.yarmitage.app.rx.RxSchedulersTransformer;
import ru.popov.bodya.yarmitage.presentation.mvp.global.view.AppPresenter;

/**
 * @author popovbodya
 */
@InjectViewState
public class ViewerPresenter extends AppPresenter<ViewerView> {

    private final List<String> mUrisList;
    private int mTargetPosition;

    public ViewerPresenter(List<String> urisList, int targetPosition) {
        mUrisList = new ArrayList<>(urisList);
        mTargetPosition = targetPosition;
    }

    @Override
    protected void onFirstViewAttach() {
        super.onFirstViewAttach();
        setupPageIndicator(mTargetPosition, getUrisCount());
        getViewState().setUriList(mUrisList);
        getViewState().selectPage(mTargetPosition);
    }

    public void onPageSelected(int position) {
        setupPageIndicator(position, getUrisCount());
    }

    private void setupPageIndicator(int position, int count) {
        final int num = position + 1;
        getViewState().setTitleText(String.format(Locale.ENGLISH, "%1$d из %2$d", num, count));
    }

    private int getUrisCount() {
        return mUrisList.size();
    }
}
