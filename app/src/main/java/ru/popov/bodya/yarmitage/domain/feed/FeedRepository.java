package ru.popov.bodya.yarmitage.domain.feed;

import java.util.List;

import io.reactivex.Single;
import ru.popov.bodya.yarmitage.domain.global.models.Resource;

/**
 * @author popovbodya
 */

public interface FeedRepository {

    Single<List<Resource>> getFilesFromNetwork(final int offset, final int limit, boolean saveLoadedData);

    Single<List<Resource>> getFilesFromCache();

}
