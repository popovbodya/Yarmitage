package ru.popov.bodya.yarmitage.presentation.ui.global;

import android.support.annotation.StringDef;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

import static ru.popov.bodya.yarmitage.presentation.ui.global.Screens.AUTH_SCREEN;
import static ru.popov.bodya.yarmitage.presentation.ui.global.Screens.FEED_SCREEN;
import static ru.popov.bodya.yarmitage.presentation.ui.global.Screens.IMAGE_VIEWER_SCREEN;
import static ru.popov.bodya.yarmitage.presentation.ui.global.Screens.YANDEX_LOGIN_SCREEN;

/**
 * @author popovbodya
 */
@Retention(RetentionPolicy.SOURCE)
@StringDef({AUTH_SCREEN, YANDEX_LOGIN_SCREEN, FEED_SCREEN, IMAGE_VIEWER_SCREEN})
public @interface Screens {

    String AUTH_SCREEN = "auth_screen";
    String YANDEX_LOGIN_SCREEN = "yandex_login_screen";
    String FEED_SCREEN = "feed_screen";
    String IMAGE_VIEWER_SCREEN = "image_viewer_screen";
    String ABOUT_SCREEN = "about_screen";

}
