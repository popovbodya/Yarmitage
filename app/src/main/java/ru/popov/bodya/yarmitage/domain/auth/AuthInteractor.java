package ru.popov.bodya.yarmitage.domain.auth;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.yandex.authsdk.YandexAuthException;
import com.yandex.authsdk.YandexAuthToken;

import io.reactivex.Completable;
import io.reactivex.Single;

/**
 * @author popovbodya
 */

public class AuthInteractor {

    private final AuthRepository mAuthRepository;

    public AuthInteractor(AuthRepository authRepository) {
        mAuthRepository = authRepository;
    }

    public Single<Intent> getAuthIntent() {
        return mAuthRepository.getAuthIntent();
    }

    public Single<YandexAuthToken> extractToken(final int resultCode, @Nullable final Intent data) throws YandexAuthException {
        return mAuthRepository.extractToken(resultCode, data);
    }

    public Completable saveToken(@NonNull YandexAuthToken yandexAuthToken) {
        return mAuthRepository.saveToken(yandexAuthToken);
    }

}
