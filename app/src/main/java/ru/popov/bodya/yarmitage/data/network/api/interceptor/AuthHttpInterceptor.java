package ru.popov.bodya.yarmitage.data.network.api.interceptor;

import java.io.IOException;

import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;

/**
 * @author popovbodya
 */

public class AuthHttpInterceptor implements Interceptor {

    private static final String TOKEN_TYPE = "OAuth";
    private final String mToken;

    public AuthHttpInterceptor(String token) {
        mToken = token;
    }

    @Override
    public Response intercept(Chain chain) throws IOException {

        final String header = String.format("%s %s", TOKEN_TYPE, mToken);
        Request request = chain.request();
        request = request.newBuilder()
                .addHeader("Authorization", header)
                .build();

        return chain.proceed(request);
    }
}
