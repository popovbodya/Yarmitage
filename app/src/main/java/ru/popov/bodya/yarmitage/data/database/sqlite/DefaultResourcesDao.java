package ru.popov.bodya.yarmitage.data.database.sqlite;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.annotation.NonNull;

import java.util.ArrayList;
import java.util.List;

import ru.popov.bodya.yarmitage.data.database.dao.ResourcesDao;
import ru.popov.bodya.yarmitage.data.database.entities.ResourceEntity;

import static ru.popov.bodya.yarmitage.data.database.sqlite.YarmitageDBContract.Resources.TABLE_NAME;

/**
 * @author popovbodya
 */

public class DefaultResourcesDao implements ResourcesDao {

    private final ResourcesSQLiteOpenHelper mResourcesSQLiteOpenHelper;

    public DefaultResourcesDao(ResourcesSQLiteOpenHelper resourcesSQLiteOpenHelper) {
        mResourcesSQLiteOpenHelper = resourcesSQLiteOpenHelper;
    }

    @Override
    public long insertResource(ResourceEntity resourceEntity) {
        long id;
        final SQLiteDatabase writableDatabase = mResourcesSQLiteOpenHelper.getWritableDatabase();
        try {
            writableDatabase.beginTransaction();
            ContentValues values = mResourcesSQLiteOpenHelper.createValuesFromResource(resourceEntity);
            id = writableDatabase.insert(TABLE_NAME, null, values);
            writableDatabase.setTransactionSuccessful();
        } finally {
            writableDatabase.endTransaction();
            writableDatabase.close();
        }
        return id;
    }

    @Override
    public long insertResourceList(List<ResourceEntity> resourceEntityList) {
        long count = 0;
        final SQLiteDatabase writableDatabase = mResourcesSQLiteOpenHelper.getWritableDatabase();
        try {
            writableDatabase.beginTransaction();
            for (ResourceEntity resourceEntity : resourceEntityList) {
                ContentValues values = mResourcesSQLiteOpenHelper.createValuesFromResource(resourceEntity);
                writableDatabase.insert(TABLE_NAME, null, values);
                count++;
            }
            writableDatabase.setTransactionSuccessful();
        } finally {
            writableDatabase.endTransaction();
            writableDatabase.close();
        }
        return count;
    }

    @Override
    public ResourceEntity getResourceById(String resourceId) {
        ResourceEntity resourceEntity = null;
        Cursor cursor = null;
        SQLiteDatabase readableDatabase = mResourcesSQLiteOpenHelper.getReadableDatabase();
        try {
            String[] selectionArgs = new String[]{resourceId};
            cursor = readableDatabase.query(TABLE_NAME, null, YarmitageDBContract.Resources.RESOURCE_ID + " = ?", selectionArgs, null, null, null);
            if (cursor.moveToFirst()) {
                resourceEntity = mResourcesSQLiteOpenHelper.createResource(cursor);
            }
        } finally {
            if (cursor != null) {
                cursor.close();
            }
            readableDatabase.close();
        }
        return resourceEntity;
    }

    @Override
    public long deleteResource(ResourceEntity resourceEntity) {
        long deletedRowsCount;
        SQLiteDatabase writableDatabase = mResourcesSQLiteOpenHelper.getWritableDatabase();
        try {
            writableDatabase.beginTransaction();
            deletedRowsCount = deleteResourceByIdInternal(resourceEntity.getResourceId(), writableDatabase);
            writableDatabase.setTransactionSuccessful();
        } finally {
            writableDatabase.endTransaction();
            writableDatabase.close();
        }
        return deletedRowsCount;
    }

    /**
     * @return the number of rows affected if a whereClause is passed in, 0 otherwise. To remove all rows and get a count pass "1" as the whereClause.
     */
    @Override
    public long deleteAllData() {
        long deletedRowsCount;
        SQLiteDatabase writableDatabase = mResourcesSQLiteOpenHelper.getWritableDatabase();
        try {
            writableDatabase.beginTransaction();
            deletedRowsCount = writableDatabase.delete(TABLE_NAME, "1", null);
            writableDatabase.setTransactionSuccessful();
        } finally {
            writableDatabase.endTransaction();
            writableDatabase.close();
        }
        return deletedRowsCount;
    }

    @Override
    public List<ResourceEntity> getResources() {
        Cursor cursor = null;
        List<ResourceEntity> resourceEntityList = new ArrayList<>();
        SQLiteDatabase db = mResourcesSQLiteOpenHelper.getReadableDatabase();
        try {
            cursor = db.query(TABLE_NAME, null, null, null, null, null, null);
            cursor.moveToFirst();
            while (!cursor.isAfterLast()) {
                resourceEntityList.add(mResourcesSQLiteOpenHelper.createResource(cursor));
                cursor.moveToNext();
            }
        } finally {
            if (cursor != null) {
                cursor.close();
            }
            db.close();
        }
        return resourceEntityList;
    }

    private int deleteResourceByIdInternal(@NonNull String resourceId, SQLiteDatabase writableDatabase) {
        String selection = YarmitageDBContract.Resources.RESOURCE_ID + " = ?";
        String[] selectionArgs = new String[]{resourceId};
        return writableDatabase.delete(YarmitageDBContract.Resources.TABLE_NAME, selection, selectionArgs);
    }
}
