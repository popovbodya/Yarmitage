package ru.popov.bodya.yarmitage.di.auth;

import android.content.Context;

import ru.popov.bodya.yarmitage.di.global.AppComponent;
import ru.popov.bodya.yarmitage.di.global.ComponentUtils;
import ru.popov.bodya.yarmitage.di.init.InitSubComponent;

/**
 * @author popovbodya
 */

public class AuthInjector {

    private static AuthSubComponent sAuthSubComponent = null;

    private AuthInjector() {
        throw new UnsupportedOperationException("No instances");
    }

    public static AuthSubComponent getAuthSubComponent(Context context) {
        if (sAuthSubComponent == null) {
            sAuthSubComponent = ComponentUtils.getComponent(context, AppComponent.class)
                    .authSubComponentBuilder()
                    .build();
        }
        return sAuthSubComponent;
    }

    public static void clearAuthSubComponent() {
        sAuthSubComponent = null;
    }


}
