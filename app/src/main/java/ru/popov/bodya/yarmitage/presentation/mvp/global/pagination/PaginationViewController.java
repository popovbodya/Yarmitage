package ru.popov.bodya.yarmitage.presentation.mvp.global.pagination;

import java.util.List;

/**
 * @author popovbodya
 */

public interface PaginationViewController<T> {

    void showFullProgress(boolean show);

    void showEmptyView(boolean show);

    void showData(boolean show, List<T> data);

    void showRefreshProgress(boolean show);

    void showPageProgress(boolean show);

    void showErrorMessage(Throwable error);
}
