package ru.popov.bodya.yarmitage.presentation.mvp.auth;

import com.arellomobile.mvp.viewstate.strategy.AddToEndSingleStrategy;
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType;

import ru.popov.bodya.yarmitage.presentation.mvp.global.view.AppView;

/**
 * @author popovbodya
 */
@StateStrategyType(AddToEndSingleStrategy.class)
public interface AuthView extends AppView {

    void showLoading(boolean show);

}
