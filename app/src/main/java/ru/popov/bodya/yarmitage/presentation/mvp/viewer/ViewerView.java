package ru.popov.bodya.yarmitage.presentation.mvp.viewer;

import java.util.List;

import ru.popov.bodya.yarmitage.presentation.mvp.global.view.AppView;

/**
 * @author popovbodya
 */

public interface ViewerView extends AppView {

    void setTitleText(String title);

    void selectPage(int position);

    void setUriList(List<String> uriList);

}
