package ru.popov.bodya.yarmitage.data.database.sqlite;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import ru.popov.bodya.yarmitage.data.database.entities.ResourceEntity;

/**
 * @author popovbodya
 */

public class ResourcesSQLiteOpenHelper extends SQLiteOpenHelper {

    static final String DB_NAME = "yarmitage.db";
    private static final int CURRENT_VERSION = 1;
    private static final String TEXT = " TEXT";
    private static final String INTEGER = " INTEGER";
    private static final String INTEGER_AUTOINCREMENT = " INTEGER PRIMARY KEY AUTOINCREMENT, ";
    private static final String DROP_TABLE = "DROP TABLE ";
    private static final String CREATE_TABLE_IF_NOT_EXISTS = "CREATE TABLE IF NOT EXISTS ";

    public ResourcesSQLiteOpenHelper(Context context) {
        super(context, DB_NAME, null, CURRENT_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        sqLiteDatabase.beginTransaction();
        try {
            createResourcesTable(sqLiteDatabase);
            sqLiteDatabase.setTransactionSuccessful();
        } finally {
            sqLiteDatabase.endTransaction();
        }
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {
        String query = DROP_TABLE + YarmitageDBContract.Resources.TABLE_NAME + ";";
        sqLiteDatabase.execSQL(query);
        onCreate(sqLiteDatabase);
    }

    ResourceEntity createResource(Cursor cursor) {
        final long id = getLong(cursor, YarmitageDBContract.Resources._ID);
        final String resourceId = getString(cursor, YarmitageDBContract.Resources.RESOURCE_ID);
        final String name = getString(cursor, YarmitageDBContract.Resources.NAME);
        final String preview = getString(cursor, YarmitageDBContract.Resources.PREVIEW);
        final String file = getString(cursor, YarmitageDBContract.Resources.FILE);
        final String path = getString(cursor, YarmitageDBContract.Resources.PATH);
        final String mediaType = getString(cursor, YarmitageDBContract.Resources.MEDIA_TYPE);
        final int size = getInt(cursor, YarmitageDBContract.Resources.SIZE);

        return new ResourceEntity(
                resourceId,
                name,
                preview,
                file,
                path,
                mediaType,
                size
        );
    }

    ContentValues createValuesFromResource(ResourceEntity resourceEntity) {
        ContentValues values = new ContentValues();
        values.put(YarmitageDBContract.Resources.RESOURCE_ID, resourceEntity.getResourceId());
        values.put(YarmitageDBContract.Resources.NAME, resourceEntity.getName());
        values.put(YarmitageDBContract.Resources.PREVIEW, resourceEntity.getPreview());
        values.put(YarmitageDBContract.Resources.FILE, resourceEntity.getFile());
        values.put(YarmitageDBContract.Resources.PATH, resourceEntity.getPath());
        values.put(YarmitageDBContract.Resources.MEDIA_TYPE, resourceEntity.getMediaType());
        values.put(YarmitageDBContract.Resources.SIZE, resourceEntity.getSize());

        return values;
    }

    private long getLong(Cursor cursor, String columnName) {
        return cursor.getLong(cursor.getColumnIndex(columnName));
    }

    private String getString(Cursor cursor, String columnName) {
        return cursor.getString(cursor.getColumnIndex(columnName));
    }

    private int getInt(Cursor cursor, String columnName) {
        return cursor.getInt(cursor.getColumnIndex(columnName));
    }


    private void createResourcesTable(SQLiteDatabase db) {
        String query = CREATE_TABLE_IF_NOT_EXISTS + YarmitageDBContract.Resources.TABLE_NAME +
                " (" +
                YarmitageDBContract.Resources._ID + INTEGER_AUTOINCREMENT +
                YarmitageDBContract.Resources.RESOURCE_ID + TEXT + ", " +
                YarmitageDBContract.Resources.NAME + TEXT + ", " +
                YarmitageDBContract.Resources.PREVIEW + TEXT + ", " +
                YarmitageDBContract.Resources.FILE + TEXT + ", " +
                YarmitageDBContract.Resources.PATH + TEXT + ", " +
                YarmitageDBContract.Resources.MEDIA_TYPE + TEXT + ", " +
                YarmitageDBContract.Resources.SIZE + INTEGER +

                ");";

        db.execSQL(query);
    }
}
