package ru.popov.bodya.yarmitage.di.global;

import javax.inject.Singleton;

import dagger.Component;
import ru.popov.bodya.yarmitage.di.auth.AuthSubComponent;
import ru.popov.bodya.yarmitage.di.feed.FeedSubComponent;
import ru.popov.bodya.yarmitage.di.global.modules.AppModule;
import ru.popov.bodya.yarmitage.di.global.modules.NavigationModule;
import ru.popov.bodya.yarmitage.di.global.modules.RxModule;
import ru.popov.bodya.yarmitage.di.init.InitSubComponent;
import ru.popov.bodya.yarmitage.di.viewer.ViewerSubComponent;

/**
 * @author popovbodya
 */
@Singleton
@Component(modules = {AppModule.class, NavigationModule.class, RxModule.class})
public interface AppComponent {

    InitSubComponent.Builder initSubComponentBuilder();

    FeedSubComponent.Builder feedSubComponentBuilder();

    AuthSubComponent.Builder authSubComponentBuilder();

    ViewerSubComponent.Builder viewerSubComponentBuilder();

}
