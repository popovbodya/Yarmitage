package ru.popov.bodya.yarmitage.di.viewer;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

import javax.inject.Scope;

/**
 * @author popovbodya
 */
@Scope
@Retention(RetentionPolicy.RUNTIME)
public @interface ViewerScope {
}
