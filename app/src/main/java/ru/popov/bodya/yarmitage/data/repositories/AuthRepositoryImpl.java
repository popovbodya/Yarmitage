package ru.popov.bodya.yarmitage.data.repositories;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.yandex.authsdk.YandexAuthException;
import com.yandex.authsdk.YandexAuthToken;

import io.reactivex.Completable;
import io.reactivex.Single;
import ru.popov.bodya.yarmitage.data.database.preferences.SharedPreferencesWrapper;
import ru.popov.bodya.yarmitage.data.network.api.sdk.YandexSdkWrapper;
import ru.popov.bodya.yarmitage.domain.auth.AuthRepository;

/**
 * @author popovbodya
 */

public class AuthRepositoryImpl implements AuthRepository {

    private final SharedPreferencesWrapper mSharedPreferencesWrapper;
    private final YandexSdkWrapper mYandexSdkWrapper;

    public AuthRepositoryImpl(SharedPreferencesWrapper sharedPreferencesWrapper, YandexSdkWrapper yandexSdkWrapper) {
        mSharedPreferencesWrapper = sharedPreferencesWrapper;
        mYandexSdkWrapper = yandexSdkWrapper;
    }

    @Override
    public Single<Intent> getAuthIntent() {
        return mYandexSdkWrapper.getAuthIntent();
    }

    @Override
    public Single<YandexAuthToken> extractToken(final int resultCode, @Nullable final Intent data) throws YandexAuthException {
        return mYandexSdkWrapper.extractToken(resultCode, data);
    }

    @Override
    public Completable saveToken(@NonNull YandexAuthToken yandexAuthToken) {
        return mSharedPreferencesWrapper.saveToken(yandexAuthToken);
    }
}
