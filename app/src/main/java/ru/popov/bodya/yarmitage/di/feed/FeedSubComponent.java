package ru.popov.bodya.yarmitage.di.feed;

import com.yandex.disk.rest.Credentials;

import dagger.BindsInstance;
import dagger.Subcomponent;
import ru.popov.bodya.yarmitage.di.feed.modules.DiskApiModule;
import ru.popov.bodya.yarmitage.di.feed.modules.FeedInteractorModule;
import ru.popov.bodya.yarmitage.di.feed.modules.FeedPresenterModule;
import ru.popov.bodya.yarmitage.di.feed.modules.FeedRepositoryModule;
import ru.popov.bodya.yarmitage.presentation.ui.feed.FeedActivity;

/**
 * @author popovbodya
 */

@FeedScope
@Subcomponent(modules = {FeedPresenterModule.class, FeedInteractorModule.class, FeedRepositoryModule.class, DiskApiModule.class,})
public interface FeedSubComponent {

    @Subcomponent.Builder
    interface Builder {

        FeedSubComponent build();

        @BindsInstance
        Builder credentials(Credentials credentials);

        @BindsInstance
        Builder token(String token);
    }

    void inject(FeedActivity feedActivity);
}
