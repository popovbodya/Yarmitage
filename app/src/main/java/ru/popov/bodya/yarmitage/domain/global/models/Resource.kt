package ru.popov.bodya.yarmitage.domain.global.models


data class Resource(
        val resourceId: String,
        val name: String,
        val preview: String?,
        val file: String,
        val path: String,
        val mediaType: String,
        val size: Int
)
