package ru.popov.bodya.yarmitage.presentation.ui.viewer.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import java.util.ArrayList;
import java.util.List;

import ru.popov.bodya.yarmitage.presentation.ui.viewer.fragment.ImageViewerPageFragment;

/**
 * @author popovbodya
 */

public class ImageViewerPagerAdapter extends FragmentStatePagerAdapter {

    private final List<String> mUrisList;

    public ImageViewerPagerAdapter(FragmentManager fm) {
        super(fm);
        mUrisList = new ArrayList<>();
    }

    @Override
    public Fragment getItem(int position) {
        final String uri = mUrisList.get(position);
        return ImageViewerPageFragment.newInstance(uri);
    }

    @Override
    public int getCount() {
        return mUrisList == null ? 0 : mUrisList.size();
    }

    public void setUrisList(List<String> urisList) {
        mUrisList.clear();
        mUrisList.addAll(urisList);
        notifyDataSetChanged();
    }
}
