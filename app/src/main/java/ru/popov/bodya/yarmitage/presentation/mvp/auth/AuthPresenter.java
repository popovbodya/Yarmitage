package ru.popov.bodya.yarmitage.presentation.mvp.auth;

import android.content.Intent;
import android.support.annotation.NonNull;

import com.arellomobile.mvp.InjectViewState;
import com.yandex.authsdk.YandexAuthException;
import com.yandex.authsdk.YandexAuthToken;

import io.reactivex.disposables.Disposable;
import ru.popov.bodya.yarmitage.app.rx.RxSchedulersTransformer;
import ru.popov.bodya.yarmitage.domain.auth.AuthInteractor;
import ru.popov.bodya.yarmitage.presentation.mvp.global.view.AppPresenter;
import ru.popov.bodya.yarmitage.presentation.ui.global.Screens;
import ru.terrakok.cicerone.Router;

import static android.app.Activity.RESULT_OK;

/**
 * @author popovbodya
 */
@InjectViewState
public class AuthPresenter extends AppPresenter<AuthView> {

    private final Router mRouter;
    private final AuthInteractor mAuthInteractor;
    private final RxSchedulersTransformer mRxSchedulersTransformer;

    public AuthPresenter(Router router, AuthInteractor authInteractor, RxSchedulersTransformer rxSchedulersTransformer) {
        super();
        mRouter = router;
        mAuthInteractor = authInteractor;
        mRxSchedulersTransformer = rxSchedulersTransformer;
    }

    @Override
    protected void onFirstViewAttach() {
        super.onFirstViewAttach();

        tryToLoginToOAuth();
    }

    public void onActivityResult(int resultCode, Intent data) {

        if (resultCode != RESULT_OK) {
            getViewState().showLoading(false);
            return;
        }

        try {

            final Disposable disposable = mAuthInteractor
                    .extractToken(resultCode, data)
                    .compose(mRxSchedulersTransformer.getIOToMainTransformerSingle())
                    .subscribe((yandexAuthToken, throwable) -> {
                        if (throwable == null && yandexAuthToken != null) {
                            onTokenReceived(yandexAuthToken);
                        }
                    });
            connect(disposable);

        } catch (YandexAuthException e) {
            e.printStackTrace();
        }
    }

    public void onEnablePermissionsButtonClick() {
        tryToLoginToOAuth();
    }

    private void onTokenReceived(@NonNull YandexAuthToken yandexAuthToken) {
        getViewState().showLoading(true);
        final Disposable disposable = mAuthInteractor
                .saveToken(yandexAuthToken)
                .compose(mRxSchedulersTransformer.getIOToMainTransformerCompletable())
                .subscribe(() -> mRouter.newRootScreen(Screens.FEED_SCREEN, yandexAuthToken.getValue()));
        connect(disposable);
    }

    private void tryToLoginToOAuth() {
        final Disposable disposable = mAuthInteractor
                .getAuthIntent()
                .compose(mRxSchedulersTransformer.getIOToMainTransformerSingle())
                .subscribe((intent, throwable) -> {
                    if (throwable == null) {
                        mRouter.navigateTo(Screens.YANDEX_LOGIN_SCREEN, intent);
                    }
                });
        connect(disposable);
    }
}
