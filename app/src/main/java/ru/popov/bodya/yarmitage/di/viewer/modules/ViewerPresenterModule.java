package ru.popov.bodya.yarmitage.di.viewer.modules;

import java.util.List;

import dagger.Module;
import dagger.Provides;
import ru.popov.bodya.yarmitage.di.viewer.ViewerScope;
import ru.popov.bodya.yarmitage.presentation.mvp.viewer.ViewerPresenter;

/**
 * @author popovbodya
 */
@Module
public class ViewerPresenterModule {

    @Provides
    @ViewerScope
    ViewerPresenter provideViewerPresenter(List<String> urisList, int targetPosition) {
        return new ViewerPresenter(urisList, targetPosition);
    }

}
