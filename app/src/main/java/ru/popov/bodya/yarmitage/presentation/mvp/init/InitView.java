package ru.popov.bodya.yarmitage.presentation.mvp.init;

import ru.popov.bodya.yarmitage.presentation.mvp.global.view.AppView;

/**
 * @author popovbodya
 */

public interface InitView extends AppView {
}
