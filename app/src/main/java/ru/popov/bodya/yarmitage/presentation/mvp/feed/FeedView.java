package ru.popov.bodya.yarmitage.presentation.mvp.feed;

import android.support.v7.util.DiffUtil;

import com.arellomobile.mvp.viewstate.strategy.AddToEndSingleStrategy;
import com.arellomobile.mvp.viewstate.strategy.OneExecutionStateStrategy;
import com.arellomobile.mvp.viewstate.strategy.SkipStrategy;
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType;

import java.util.List;

import ru.popov.bodya.yarmitage.domain.global.models.Resource;
import ru.popov.bodya.yarmitage.presentation.mvp.global.view.AppView;

/**
 * @author popovbodya
 */
@StateStrategyType(AddToEndSingleStrategy.class)
public interface FeedView extends AppView {

    @StateStrategyType(SkipStrategy.class)
    void takeData(boolean show, List<Resource> resourcesList);

    void showDiffData(List<Resource> newList, DiffUtil.DiffResult diffResult);

    void showRefreshProgress(boolean show);

    void showFullProgress(boolean show);

    void showPageProgress(boolean show);

    void showZeroView(boolean show);

    @StateStrategyType(OneExecutionStateStrategy.class)
    void showErrorMessage();

}
