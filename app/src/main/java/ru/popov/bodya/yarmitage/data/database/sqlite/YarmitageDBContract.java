package ru.popov.bodya.yarmitage.data.database.sqlite;

/**
 * @author popovbodya
 */
class YarmitageDBContract {

    private YarmitageDBContract() {
    }

    // Resources
    static final class Resources {

        static final String TABLE_NAME = "resources";

        static final String _ID = "_id";
        static final String RESOURCE_ID = "resourceId";
        static final String NAME = "name";
        static final String PREVIEW = "preview";
        static final String FILE = "file";
        static final String PATH = "path";
        static final String MEDIA_TYPE = "mediaType";
        static final String SIZE = "size";


        static final String[] ALL_COLUMNS = new String[]{
                _ID,
                RESOURCE_ID,
                NAME,
                PREVIEW,
                FILE,
                PATH,
                MEDIA_TYPE,
                SIZE,
        };

        private Resources() {
        }
    }
}
