package ru.popov.bodya.yarmitage.presentation.mvp.global.pagination;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import io.reactivex.disposables.Disposable;

/**
 * @author popovbodya
 */

public class Paginator<T> {

    private static final int FIRST_PAGE = 0;
    private final RequestFactory<T> mRequestFactory;
    private final PaginationViewController<T> mPaginationViewController;

    private State<T> mCurrentState;
    private int mCurrentPage;
    private List<T> mCurrentData;
    private Disposable mDisposable;

    public Paginator(RequestFactory<T> requestFactory, PaginationViewController<T> paginationViewController) {
        mRequestFactory = requestFactory;
        mPaginationViewController = paginationViewController;
        mCurrentState = new EmptyState();
        mCurrentPage = FIRST_PAGE;
        mCurrentData = new ArrayList<>();
    }

    public void refresh() {
        mCurrentState.refresh();
    }

    public void loadNewPage() {
        mCurrentState.loadNewPage();
    }

    public void release() {
        mCurrentState.release();
    }

    private void initialLoad() {
        if (mDisposable != null) {
            mDisposable.dispose();
        }
        mDisposable = mRequestFactory
                .makeInitialRequest()
                .subscribe(
                        data -> mCurrentState.newData(data),
                        throwable -> mCurrentState.fail(throwable)
                );
    }

    private void loadPage(int page) {
        if (mDisposable != null) {
            mDisposable.dispose();
        }
        mDisposable = mRequestFactory
                .makeRequest(page)
                .subscribe(
                        data -> mCurrentState.newData(data),
                        throwable -> mCurrentState.fail(throwable)
                );
    }

    private void releaseState() {
        mCurrentState = new ReleasedState();
        if (mDisposable != null) {
            mDisposable.dispose();
        }
    }

    private class EmptyState implements State<T> {
        @Override
        public void refresh() {
            mCurrentState = new FullProgressState();
            mPaginationViewController.showFullProgress(true);
            initialLoad();
        }

        @Override
        public void release() {
            releaseState();
        }
    }

    private class FullProgressState implements State<T> {

        @Override
        public void newData(List<T> data) {
            if (!data.isEmpty()) {
                mCurrentPage = FIRST_PAGE;
                mCurrentState = new DataState();
                mCurrentData.clear();
                mCurrentData.addAll(data);
                mPaginationViewController.showData(true, mCurrentData);
            } else {
                mCurrentState = new EmptyDataState();
                mPaginationViewController.showEmptyView(true);
            }
            mPaginationViewController.showFullProgress(false);
        }

        @Override
        public void fail(Throwable error) {
            error.printStackTrace();
            mCurrentState = new EmptyDataState();
            mPaginationViewController.showFullProgress(false);
            mPaginationViewController.showEmptyView(true);
        }

        @Override
        public void release() {
            releaseState();
        }
    }

    private class EmptyDataState implements State<T> {

        @Override
        public void refresh() {
            mCurrentState = new FullProgressState();
            mPaginationViewController.showEmptyView(false);
            mPaginationViewController.showFullProgress(true);
            loadPage(FIRST_PAGE);
        }

        @Override
        public void release() {
            releaseState();
        }
    }

    private class DataState implements State<T> {

        @Override
        public void refresh() {
            mCurrentState = new RefreshState();
            mPaginationViewController.showRefreshProgress(true);
            loadPage(FIRST_PAGE);
        }

        @Override
        public void loadNewPage() {
            mCurrentState = new PageProgressState();
            mPaginationViewController.showPageProgress(true);
            loadPage(mCurrentPage + 1);
        }

        @Override
        public void newData(List<T> data) {
            if (!data.isEmpty()) {
                mCurrentPage = FIRST_PAGE;
                mCurrentData.clear();
                mCurrentData.addAll(data);
                mPaginationViewController.showData(true, mCurrentData);
            }
        }

        @Override
        public void fail(Throwable error) {
            mPaginationViewController.showErrorMessage(error);
        }

        @Override
        public void release() {
            releaseState();
        }
    }

    private class RefreshState implements State<T> {

        @Override
        public void newData(List<T> data) {
            if (!data.isEmpty()) {
                mCurrentState = new DataState();
                mCurrentData.clear();
                mCurrentData.addAll(data);
                mCurrentPage = FIRST_PAGE;
                mPaginationViewController.showRefreshProgress(false);
                mPaginationViewController.showData(true, mCurrentData);
            } else {
                mCurrentState = new EmptyDataState();
                mCurrentData.clear();
                mPaginationViewController.showData(false, Collections.emptyList());
                mPaginationViewController.showRefreshProgress(false);
                mPaginationViewController.showEmptyView(true);
            }
        }

        @Override
        public void fail(Throwable error) {
            mCurrentState = new DataState();
            mPaginationViewController.showRefreshProgress(false);
            mPaginationViewController.showErrorMessage(error);
        }

        @Override
        public void release() {
            releaseState();
        }
    }

    private class PageProgressState implements State<T> {

        @Override
        public void newData(List<T> data) {
            if (!data.isEmpty()) {
                mCurrentState = new DataState();
                mCurrentData.addAll(data);
                mCurrentPage++;
                mPaginationViewController.showPageProgress(false);
                mPaginationViewController.showData(true, mCurrentData);
            } else {
                mCurrentState = new AllDataState();
                mPaginationViewController.showPageProgress(false);
            }
        }

        @Override
        public void refresh() {
            mCurrentState = new RefreshState();
            mPaginationViewController.showPageProgress(false);
            mPaginationViewController.showRefreshProgress(true);
            loadPage(FIRST_PAGE);
        }

        @Override
        public void fail(Throwable error) {
            mCurrentState = new DataState();
            mPaginationViewController.showPageProgress(false);
            mPaginationViewController.showErrorMessage(error);
        }

        @Override
        public void release() {
            releaseState();
        }
    }

    private class AllDataState implements State<T> {

        @Override
        public void refresh() {
            mCurrentState = new RefreshState();
            mPaginationViewController.showRefreshProgress(true);
            loadPage(FIRST_PAGE);
        }

        @Override
        public void release() {
            releaseState();
        }
    }

    private class ReleasedState implements State<T> {
    }


}
