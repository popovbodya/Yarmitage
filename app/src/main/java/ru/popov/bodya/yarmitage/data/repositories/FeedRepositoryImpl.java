package ru.popov.bodya.yarmitage.data.repositories;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import io.reactivex.Single;
import ru.popov.bodya.yarmitage.data.database.converter.ResourceEntityConverter;
import ru.popov.bodya.yarmitage.data.database.dao.ResourcesDao;
import ru.popov.bodya.yarmitage.data.network.api.YandexDiskApi;
import ru.popov.bodya.yarmitage.data.network.converter.ResourceConverter;
import ru.popov.bodya.yarmitage.domain.feed.FeedRepository;
import ru.popov.bodya.yarmitage.domain.global.models.Resource;

/**
 * @author popovbodya
 */

public class FeedRepositoryImpl implements FeedRepository {

    private final YandexDiskApi mYandexDiskApi;
    private final ResourcesDao mResourcesDao;
    private final ResourceEntityConverter mResourceEntityConverter;
    private final ResourceConverter mResourceConverter;

    public FeedRepositoryImpl(YandexDiskApi yandexDiskApi, ResourcesDao resourcesDao, ResourceEntityConverter resourceEntityConverter, ResourceConverter resourceConverter) {
        mYandexDiskApi = yandexDiskApi;
        mResourcesDao = resourcesDao;
        mResourceEntityConverter = resourceEntityConverter;
        mResourceConverter = resourceConverter;
    }

    @Override
    public Single<List<Resource>> getFilesFromNetwork(final int offset, final int limit, boolean saveLoadedData) {
        return mYandexDiskApi
                .getFiles(getFilesParams(limit, offset))
                .map(mResourceConverter::convertList)
                .doOnSuccess(resources -> {
                    if (saveLoadedData) {
                        mResourcesDao.deleteAllData();
                        mResourcesDao.insertResourceList(mResourceEntityConverter.convertList(resources));
                    }
                });
    }

    @Override
    public Single<List<Resource>> getFilesFromCache() {
        return Single
                .fromCallable(mResourcesDao::getResources)
                .map(mResourceEntityConverter::reverseList);
    }

    private Map<String, String> getFilesParams(int limit, int offset) {
        final Map<String, String> map = new HashMap<>();
        map.put("media_type", "image");
        map.put("preview_size", "M");
        map.put("limit", String.valueOf(limit));
        map.put("offset", String.valueOf(offset));
        map.put("sort", "-modified");
        return map;
    }

}
