package ru.popov.bodya.yarmitage.data.network.api.sdk;


import android.content.Context;
import android.content.Intent;
import android.support.annotation.Nullable;

import com.yandex.authsdk.YandexAuthException;
import com.yandex.authsdk.YandexAuthSdk;
import com.yandex.authsdk.YandexAuthToken;

import io.reactivex.Single;
import ru.popov.bodya.yarmitage.di.global.ApplicationContext;

public class YandexSdkWrapper {

    @ApplicationContext
    private final Context mAppContext;
    private final YandexAuthSdk mYandexAuthSdk;

    public YandexSdkWrapper(Context appContext, YandexAuthSdk yandexAuthSdk) {
        mAppContext = appContext;
        mYandexAuthSdk = yandexAuthSdk;
    }

    public Single<Intent> getAuthIntent() {
        return Single.fromCallable(() -> mYandexAuthSdk.createLoginIntent(mAppContext, null));
    }

    public Single<YandexAuthToken> extractToken(final int resultCode, @Nullable final Intent data) throws YandexAuthException {
        return Single.fromCallable(() -> mYandexAuthSdk.extractToken(resultCode, data));
    }
}
