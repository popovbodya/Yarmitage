package ru.popov.bodya.yarmitage.di.auth;

import dagger.Subcomponent;
import ru.popov.bodya.yarmitage.di.auth.modules.AuthInteractorModule;
import ru.popov.bodya.yarmitage.di.auth.modules.AuthPresenterModule;
import ru.popov.bodya.yarmitage.di.auth.modules.AuthRepositoryModule;
import ru.popov.bodya.yarmitage.di.global.modules.RxModule;
import ru.popov.bodya.yarmitage.domain.auth.AuthRepository;
import ru.popov.bodya.yarmitage.presentation.ui.auth.activity.AuthActivity;

/**
 * @author popovbodya
 */
@AuthScope
@Subcomponent(modules = {AuthPresenterModule.class, AuthRepositoryModule.class, AuthInteractorModule.class})
public interface AuthSubComponent {

    @Subcomponent.Builder
    interface Builder {
        AuthSubComponent build();
    }

    void inject(AuthActivity authActivity);

}
