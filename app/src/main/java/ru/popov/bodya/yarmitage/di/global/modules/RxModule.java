package ru.popov.bodya.yarmitage.di.global.modules;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import ru.popov.bodya.yarmitage.app.rx.RxSchedulers;
import ru.popov.bodya.yarmitage.app.rx.RxSchedulersImpl;
import ru.popov.bodya.yarmitage.app.rx.RxSchedulersTransformer;
import ru.popov.bodya.yarmitage.app.rx.RxSchedulersTransformerImpl;

/**
 * @author popovbodya
 */

@Module
public class RxModule {

    @Singleton
    @Provides
    RxSchedulersTransformer provideRxSchedulersTransformer(RxSchedulers rxSchedulers) {
        return new RxSchedulersTransformerImpl(rxSchedulers);
    }

    @Singleton
    @Provides
    RxSchedulers provideRxSchedulers() {
        return new RxSchedulersImpl();
    }
}
