package ru.popov.bodya.yarmitage.presentation.ui.global;

import android.support.v4.view.ViewPager;

/**
 * @author popovbodya
 */

public class OnPageChangeAdapter implements ViewPager.OnPageChangeListener {
    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    @Override
    public void onPageSelected(int position) {

    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }
}
