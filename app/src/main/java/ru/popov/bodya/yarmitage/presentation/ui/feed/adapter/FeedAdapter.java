package ru.popov.bodya.yarmitage.presentation.ui.feed.adapter;

import android.support.v7.util.DiffUtil;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bumptech.glide.RequestManager;
import com.bumptech.glide.load.model.LazyHeaders;
import com.bumptech.glide.request.RequestOptions;

import java.util.ArrayList;
import java.util.List;

import ru.popov.bodya.yarmitage.R;
import ru.popov.bodya.yarmitage.domain.global.models.Resource;
import ru.popov.bodya.yarmitage.presentation.ui.feed.adapter.holder.ResourceViewHolder;

/**
 * @author popovbodya
 */

public class FeedAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private final List<Resource> mResourceList;
    private final RequestManager mRequestManager;
    private final RequestOptions mRequestOptions;
    private final OnImageClickListener mOnImageClickListener;
    private final OnLoadMoreListener mOnLoadMoreListener;
    private final LazyHeaders mLazyHeaders;

    public FeedAdapter(RequestManager requestManager, RequestOptions requestOptions, LazyHeaders lazyHeaders, OnImageClickListener onImageClickListener, OnLoadMoreListener onLoadMoreListener) {
        mRequestManager = requestManager;
        mRequestOptions = requestOptions;
        mLazyHeaders = lazyHeaders;
        mResourceList = new ArrayList<>();
        mOnImageClickListener = onImageClickListener;
        mOnLoadMoreListener = onLoadMoreListener;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.feed_grid_cell, parent, false);
        return new ResourceViewHolder(view, mLazyHeaders, mRequestManager, mRequestOptions, mOnImageClickListener);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        final Resource resource = mResourceList.get(position);
        ((ResourceViewHolder) holder).bind(resource.getPreview(), position);

        if (position == mResourceList.size() - 1) {
            mOnLoadMoreListener.onLoadMore();
        }
    }

    @Override
    public int getItemCount() {
        return mResourceList.size();

    }

    public void setList(List<? extends Resource> resourceList, DiffUtil.DiffResult diffResult) {

        mResourceList.clear();
        mResourceList.addAll(resourceList);
        diffResult.dispatchUpdatesTo(this);
    }

    public List<Resource> getCurrentList() {
        return mResourceList;
    }

    public List<String> getUriList() {
        final List<String> urisList = new ArrayList<>(mResourceList.size());
        for (Resource resource : mResourceList) {
            urisList.add(resource.getFile());
        }
        return urisList;
    }

    public interface OnImageClickListener {
        void onImageClick(int position);
    }

    public interface OnLoadMoreListener {
        void onLoadMore();
    }
}
