package ru.popov.bodya.yarmitage.data.database.preferences;


import android.content.SharedPreferences;
import android.content.pm.ApplicationInfo;
import android.support.annotation.NonNull;

import com.yandex.authsdk.YandexAuthToken;
import com.yandex.authsdk.internal.Constants;

import io.reactivex.Completable;

public class SharedPreferencesWrapper {

    private static final String CLIENT_ID_KEY = "com.yandex.auth.CLIENT_ID";

    private final SharedPreferences mSharedPreferences;
    private final ApplicationInfo mApplicationInfo;

    public SharedPreferencesWrapper(SharedPreferences sharedPreferences, ApplicationInfo applicationInfo) {
        mSharedPreferences = sharedPreferences;
        mApplicationInfo = applicationInfo;
    }

    public String getToken() {
        return mSharedPreferences.getString(getClientId(), "");
    }

    public Completable saveToken(@NonNull YandexAuthToken yandexAuthToken) {
        return Completable.fromRunnable(() -> {
            SharedPreferences.Editor editor = mSharedPreferences.edit();
            editor.putString(getClientId(), yandexAuthToken.getValue());
            editor.apply();
        });
    }

    @NonNull
    private String getClientId() {
        String clientId = (String) mApplicationInfo.metaData.get(CLIENT_ID_KEY);
        if (clientId == null) {
            throw new IllegalStateException(
                    String.format("Application should provide %s in AndroidManifest.xml",
                            Constants.META_CLIENT_ID));
        }
        return clientId;
    }

}
