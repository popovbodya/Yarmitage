package ru.popov.bodya.yarmitage.di.viewer;

import java.util.List;

import dagger.BindsInstance;
import dagger.Subcomponent;
import ru.popov.bodya.yarmitage.di.viewer.modules.ViewerPresenterModule;
import ru.popov.bodya.yarmitage.presentation.ui.viewer.activity.ImageViewerActivity;

/**
 * @author popovbodya
 */
@ViewerScope
@Subcomponent(modules = {ViewerPresenterModule.class})
public interface ViewerSubComponent {

    @Subcomponent.Builder
    interface Builder {
        ViewerSubComponent build();

        @BindsInstance
        Builder urisList(List<String> urisList);

        @BindsInstance
        Builder targetPosition(int targetPosition);
    }

    void inject(ImageViewerActivity initActivity);

}