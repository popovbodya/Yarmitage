package ru.popov.bodya.yarmitage.app.rx;

import android.support.annotation.NonNull;

import io.reactivex.Scheduler;

/**
 * @author popovbodya
 */

public interface RxSchedulers {

    @NonNull
    Scheduler getMainThreadScheduler();

    @NonNull
    Scheduler getIOScheduler();

    @NonNull
    Scheduler getComputationScheduler();

}
