package ru.popov.bodya.yarmitage.di.global;

/**
 * @author popovbodya
 */

public interface IHasComponent<T> {

    T getComponent();

}
