package ru.popov.bodya.yarmitage.domain.auth;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.yandex.authsdk.YandexAuthException;
import com.yandex.authsdk.YandexAuthToken;

import io.reactivex.Completable;
import io.reactivex.Single;

/**
 * @author popovbodya
 */

public interface AuthRepository {

    Single<Intent> getAuthIntent();

    Single<YandexAuthToken> extractToken(final int resultCode, @Nullable final Intent data) throws YandexAuthException;

    Completable saveToken(@NonNull YandexAuthToken yandexAuthToken);

}
