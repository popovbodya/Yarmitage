package ru.popov.bodya.yarmitage.di.auth.modules;

import dagger.Module;
import dagger.Provides;
import ru.popov.bodya.yarmitage.di.auth.AuthScope;
import ru.popov.bodya.yarmitage.domain.auth.AuthInteractor;
import ru.popov.bodya.yarmitage.domain.auth.AuthRepository;

/**
 * @author popovbodya
 */
@Module
public class AuthInteractorModule {

    @AuthScope
    @Provides
    AuthInteractor provideAuthInteractor(AuthRepository authRepository) {
        return new AuthInteractor(authRepository);
    }

}
