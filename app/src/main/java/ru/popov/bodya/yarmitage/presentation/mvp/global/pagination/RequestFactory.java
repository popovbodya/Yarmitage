package ru.popov.bodya.yarmitage.presentation.mvp.global.pagination;

import java.util.List;

import io.reactivex.Flowable;
import io.reactivex.Single;

/**
 * @author popovbodya
 */
public interface RequestFactory<T> {

    Single<List<T>> makeRequest(int page);

    Flowable<List<T>> makeInitialRequest();

}
