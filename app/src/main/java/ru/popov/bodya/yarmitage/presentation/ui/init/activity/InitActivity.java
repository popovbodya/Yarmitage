package ru.popov.bodya.yarmitage.presentation.ui.init.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;

import com.arellomobile.mvp.presenter.InjectPresenter;
import com.arellomobile.mvp.presenter.ProvidePresenter;

import javax.inject.Inject;

import ru.popov.bodya.yarmitage.di.init.InitInjector;
import ru.popov.bodya.yarmitage.presentation.mvp.init.InitPresenter;
import ru.popov.bodya.yarmitage.presentation.mvp.init.InitView;
import ru.popov.bodya.yarmitage.presentation.ui.auth.activity.AuthActivity;
import ru.popov.bodya.yarmitage.presentation.ui.feed.FeedActivity;
import ru.popov.bodya.yarmitage.presentation.ui.global.BaseCoreActivity;
import ru.popov.bodya.yarmitage.presentation.ui.global.Screens;
import ru.terrakok.cicerone.Navigator;
import ru.terrakok.cicerone.NavigatorHolder;
import ru.terrakok.cicerone.android.SupportAppNavigator;

/**
 * @author popovbodya
 */

public class InitActivity extends BaseCoreActivity implements InitView {

    @Inject
    @InjectPresenter
    InitPresenter mInitPresenter;
    @Inject
    NavigatorHolder mNavigatorHolder;

    @ProvidePresenter
    public InitPresenter provideInitPresenter() {
        return mInitPresenter;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        InitInjector
                .getFeedSubComponent(this)
                .inject(this);
        super.onCreate(savedInstanceState);
    }

    @Override
    protected void onResume() {
        super.onResume();
        mNavigatorHolder.setNavigator(mNavigator);
    }

    @Override
    protected void onPause() {
        mNavigatorHolder.removeNavigator();
        super.onPause();
    }

    @Override
    protected void onDestroy() {
        if (isFinishing()) {
            InitInjector.clearInitSubComponent();
        }
        super.onDestroy();
    }

    private Navigator mNavigator = new SupportAppNavigator(this, 0) {
        @Override
        protected Intent createActivityIntent(Context context, String screenKey, Object data) {
            switch (screenKey) {
                case Screens.AUTH_SCREEN:
                    return new Intent(InitActivity.this, AuthActivity.class);
                case Screens.FEED_SCREEN:
                    final Intent intent = new Intent(InitActivity.this, FeedActivity.class);
                    intent.putExtra(FeedActivity.ARG_TOKEN_KEY, (String) data);
                    return intent;
                default:
                    return null;
            }
        }

        @Override
        protected Fragment createFragment(String screenKey, Object data) {
            return null;
        }
    };

}
