package ru.popov.bodya.yarmitage.di.feed.modules;

import com.yandex.disk.rest.Credentials;
import com.yandex.disk.rest.RestClient;

import dagger.Module;
import dagger.Provides;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.CallAdapter;
import retrofit2.Converter;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;
import ru.popov.bodya.yarmitage.data.network.api.YandexDiskApi;
import ru.popov.bodya.yarmitage.data.network.api.interceptor.AuthHttpInterceptor;
import ru.popov.bodya.yarmitage.di.feed.FeedScope;

import static ru.popov.bodya.yarmitage.data.network.api.constants.DiskApiConstants.BASE_DISK_API_URL;

/**
 * @author popovbodya
 */
@Module
public class DiskApiModule {

    @Provides
    @FeedScope
    YandexDiskApi provideYandexDiskApi(OkHttpClient okHttpClient,
                                       Converter.Factory converterFactory,
                                       CallAdapter.Factory adapterFactory) {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(BASE_DISK_API_URL)
                .addConverterFactory(converterFactory)
                .addCallAdapterFactory(adapterFactory)
                .client(okHttpClient)
                .build();
        return retrofit.create(YandexDiskApi.class);
    }

    @Provides
    @FeedScope
    RestClient provideRestClient(Credentials credentials) {
        return new RestClient(credentials);
    }

    @Provides
    @FeedScope
    OkHttpClient provideOkHttpClient(HttpLoggingInterceptor httpLoggingInterceptor, AuthHttpInterceptor authHttpInterceptor) {
        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();

        httpClient.addInterceptor(httpLoggingInterceptor);
        httpClient.addInterceptor(authHttpInterceptor);

        return httpClient.build();
    }

    @Provides
    @FeedScope
    CallAdapter.Factory provideCallAdapterFactory() {
        return RxJava2CallAdapterFactory.create();
    }

    @Provides
    @FeedScope
    Converter.Factory provideConverterFactory() {
        return GsonConverterFactory.create();
    }

    @Provides
    @FeedScope
    AuthHttpInterceptor provideAuthInterceptor(Credentials credentials) {
        return new AuthHttpInterceptor(credentials.getToken());
    }

    @Provides
    @FeedScope
    HttpLoggingInterceptor provideHttpLoggingInterceptor() {
        final HttpLoggingInterceptor httpLoggingInterceptor = new HttpLoggingInterceptor();
        httpLoggingInterceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        return httpLoggingInterceptor;
    }
}
