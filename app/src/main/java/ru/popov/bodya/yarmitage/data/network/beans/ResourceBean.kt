package ru.popov.bodya.yarmitage.data.network.beans

import com.google.gson.annotations.SerializedName
import java.util.*

/**
 * @author popovbodya
 */
data class ResourceBean(
        @SerializedName("resource_id") val resourceId: String,
        @SerializedName("name") val name: String,
        @SerializedName("preview") val preview: String,
        @SerializedName("file") val file: String,
        @SerializedName("path") val path: String,
        @SerializedName("media_type") val mediaType: String,
        @SerializedName("size") val size: Int
)